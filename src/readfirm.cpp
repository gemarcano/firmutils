// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020

#include <firmutils/firm.h>
#include <firmutils/big_unsigned.h>
#include <firmutils/elf_utils.h>
#include <firmutils/json_utils.h>
#include <firmutils/log.h>

#include <cxxopts.hpp>
#include <nlohmann/json.hpp>

#include <iostream>
#include <array>
#include <iomanip>
#include <fstream>
#include <vector>
#include <filesystem>
#include <map>
#include <set>

/** Represents options passed into the program */
struct program_options
{
	/** Input config path */
	std::optional<std::filesystem::path> firm_path;
	/** Whether to read from standard input instead of a file or not */
	bool standard_input;
	/** Log level */
	firmutils::log_level verbose;
	/** Whether help was requested or not */
	bool help;
};

// Helper functions, pull out some functionality out of main() to make it
// easier to reason about

/** Prepares CLI argument handling.
 *
 * @returns An Options object with parameters set up for parsing argc and argv.
 */
static cxxopts::Options prepare_cli();

/** Takes in the program arguments, and converts that information into a
 * program_options structure for more structured access.
 *
 * This function does not do much checking of the values of the options, it
 * merely extracts them. check_program_options() checks consistency.
 *
 * @param[in] opts Options from cxxopts.
 * @param[in] argc Number of arguments (main argc).
 * @param[out] argv Array of string arguments (main argv).
 *
 * @returns The program options, parsed, plus all messages captured/generated
 *  while parsing. A failure is indicated by the presence of at least one
 *  log_level::FATAL message.
 */
static firmutils::logged_result_type<program_options> process_opts(
	cxxopts::Options& opts, int argc, char *argv[]);

/** Checks the consistency of the program arguments, as parsed into a
 * program_options structure.
 *
 * @param[in] program_options_ Program options to check.
 *
 * @returns A log_messages container with no log_level::FATAL messages on
 *  success, any log_level::FATAL messages in the container indicates a
 *  failure.
 */
static firmutils::log_messages check_program_options(
	const program_options& program_options_);

int main(int argc, char *argv[])
{
	// Stage 1: Set up parameter handling
	auto opts = prepare_cli();

	// Stage 2: Process CLI parameters, returns program_options
	auto opts_result = process_opts(opts, argc, argv);
	auto program_options_ = opts_result.get();

	// Stage 2.5: Now check results of stage 2
	firmutils::log_level error_level = firmutils::log_level::FATAL;

	// Bail out early if the user is asking for help
	if (program_options_.help)
	{
		std::cout << opts.help() << std::endl;
		return 0;
	}

	auto program_messages = check_program_options(program_options_);
	for (auto& msg : program_messages)
		logging(msg.first) << msg.second << std::endl;

	firmutils::logged_result_type<bool> program_result;
	program_result.messages() = std::move(program_messages);
	if (program_result.at_least(error_level))
	{
		return 1;
	}

	firmutils::firm firm{};
	std::ifstream firm_file;
	if (program_options_.firm_path)
		firm_file.open(*program_options_.firm_path, std::ios::binary);

	std::istream &is = program_options_.standard_input ? std::cin : firm_file;
	is >> firm;
	if (is)
	{
		std::cout << "FIRM details:" << std::endl;
		std::cout << firmutils::to_string(firm.header) << std::endl;
		std::optional<firmutils::firm_section_encryption> encryption =
			firmutils::guess_firm_encryption(firm);
		firmutils::logged_result_type<bool> check;
		check.get() = false;
		if (encryption)
		{
			check.messages() = firmutils::check_firm(firm, *encryption);
			check.get() = check.at_least(error_level);
		}
		std::cout << "Valid FIRM: " << (check ? "yes" : "no") << std::endl;
		if (!check)
		{
			std::cout << "Reason: " << std::endl;
			for (auto& msg : check.messages())
				if (msg.first == firmutils::log_level::FATAL)
					std::cout << "    " << msg.second << std::endl;
		}
		else
		{
			bool bootable = !check.at_level(firmutils::log_level::WARN);
			std::cout << "Will it boot? " << (bootable ? "yes" : "no") <<
				std::endl;
			if (!bootable)
			{
				std::cout << "Reason: " << std::endl;
				for (auto& msg : check.messages())
					if (msg.first == firmutils::log_level::WARN)
						std::cout << "    " << msg.second << std::endl;
			}
		}
	}
	else
	{
		firmutils::logging(firmutils::log_level::FATAL) <<
			"Error while parsing FIRM" << std::endl;
		if (is.eof())
			firmutils::logging(firmutils::log_level::FATAL) <<
				"End of file encountered" << std::endl;
		if (is.bad())
			firmutils::logging(firmutils::log_level::FATAL) <<
				"IO error occurred" << std::endl;
		if (is.fail())
			firmutils::logging(firmutils::log_level::FATAL) <<
				"Parsing error occurred" << std::endl;

	}
	return 0;
}

cxxopts::Options prepare_cli()
{
	const char *description = "Program to read 3DS FIRM images";
	cxxopts::Options opts("readfirm", description);
	opts.add_options()
		("f,firm", "FIRM to parse",
		 cxxopts::value<std::vector<std::string>>())
		("s,stdin", "Read from standard input instead of a file")
		("v,verbose", "Level of verbosity. Defaults to INFO if not specified, "
		 "VERBOSE if specified without an argument. Possible values are: [ "
		 "FATAL, WARN, INFO, VERBOSE, DEBUG ]",
			cxxopts::value<std::string>()->implicit_value("VERBOSE"))
		("h,help", "Print this help information")
	;
	opts.positional_help("[FIRM]");

	return opts;
}

static firmutils::result_type<cxxopts::ParseResult, firmutils::log_messages>
	get_opts(cxxopts::Options& opts, int argc, char *argv[])
{
	// We need to do this convoluted mess because cxxopts doesn't provide a
	// default constructor for ParseResult
	opts.parse_positional("firm");
	try
	{
		return firmutils::result_type<
			cxxopts::ParseResult, firmutils::log_messages>(
				opts.parse(argc, const_cast<const char**&>(argv)));
	}
	catch(cxxopts::exceptions::exception& e)
	{
		// Since most (all?) params are strings, this should pretty much almost
		// never trigger now...
		return firmutils::result_type<
			cxxopts::ParseResult, firmutils::log_messages>(
				firmutils::log_messages{
					{firmutils::log_level::FATAL, e.what()}});
	}
}

static firmutils::logged_result_type<program_options>
	process_opts(cxxopts::Options& opts, int argc, char *argv[])
{
	auto opts_get_result = get_opts(opts, argc, argv);
	if (!opts_get_result)
	{
		return firmutils::logged_result_type<program_options>(
			program_options{}, opts_get_result.error());
	}

	firmutils::logged_result_type<program_options> result{};
	program_options &result_data = result.get();
	firmutils::log_messages &messages = result.messages();

	cxxopts::ParseResult& opts_result = opts_get_result.get();

	if (opts_result.count("help"))
	{
		result_data.help = true;
	}

	result_data.verbose = firmutils::log_level::INFO;
	if (opts_result.count("verbose"))
	{
		std::string verbose = opts_result["verbose"].as<std::string>();
		try
		{
			result_data.verbose =
				firmutils::from_string<firmutils::log_level>(verbose);
		}
		catch (std::invalid_argument&)
		{
			std::stringstream ss;
			ss << "Invalid verbose parameter: " << verbose;
			messages.emplace_back(firmutils::log_level::FATAL, ss.str());
		}
	}
	if (opts_result.count("firm"))
	{
		// FIXME do we want to limit it to just one FIRM, or all given in the CLI?
		if (opts_result["firm"].as<std::vector<std::string>>().size() == 1)
		{
			std::filesystem::path firm_path =
				opts_result["firm"].as<std::vector<std::string>>()[0];
			result_data.firm_path = firm_path;
		}
		else
		{
			messages.emplace_back(firmutils::log_level::FATAL,
				"Too many positional parameters specified, only one accepted");
		}
	}
	result_data.standard_input = opts_result.count("stdin");

	return result;
}

static firmutils::log_messages check_program_options(
		const program_options& program_options_)
{
	firmutils::log_messages result{};
	auto& messages = result;
	// Begin checking parameters
	// FIXME is this true for readfirm?
	if (program_options_.firm_path && program_options_.standard_input)
	{
		messages.emplace_back(firmutils::log_level::FATAL,
			"Can't read both from a FIRM file and stdin, select only one");
	}
	else if (!program_options_.firm_path && !program_options_.standard_input)
	{
		messages.emplace_back(firmutils::log_level::FATAL,
			"Need either a FIRM file or data from stdin");
	}
	else if (program_options_.firm_path)
	{
		if (!std::filesystem::is_regular_file(*program_options_.firm_path))
		{
			std::stringstream ss;
			ss << "File :" << *program_options_.firm_path <<
				" is not a valid file";
			messages.emplace_back(firmutils::log_level::FATAL, ss.str());
		}
	}

	return result;
}

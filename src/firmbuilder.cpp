// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020

#include <firmutils/firm.h>
#include <firmutils/big_unsigned.h>
#include <firmutils/elf_utils.h>
#include <firmutils/json_utils.h>
#include <firmutils/log.h>

#include <cxxopts.hpp>
#include <nlohmann/json.hpp>

#include <iostream>
#include <array>
#include <iomanip>
#include <fstream>
#include <vector>
#include <filesystem>
#include <map>
#include <set>

using namespace firmutils;

/** Represents options passed into the program */
struct program_options
{
	/** Output FIRM path */
	std::filesystem::path output;
	/** Input config path */
	std::optional<std::filesystem::path> config;
	/** Whether to force overwritting output or not */
	bool force;
	/** Whether to read from standard input instead of a file or not */
	bool standard_input;
	/** Log level */
	log_level verbose;
	/** Whether help was requested or not */
	bool help;
	/** Make warnings errors. */
	bool error_warnings;
	/** Try building FIRM even if there are errors checking it */
	bool force_build;
	/** JSON representation of FIRM header information provided as parameters,
	 * to override fields in the configuration file, if given.
	 */
	nlohmann::json overrides;
};

/** Represents the program's actual view of FIRM section headers */
struct internal_section_header
{
	/** The load address of the section. This is optional if the file is an
	 * ELF, as the load address can be extracted from the first code segment in
	 * order. This is mandatory for raw data files.
	 */
	std::optional<uint32_t> load_address;
	/** The copy method to use for loading FIRM */
	firm_copy_method copy_method;
	/** The path to the file to use as data */
	std::filesystem::path file_path;
};

/** Represents the program's actual view of FIRM headers */
struct internal_firm
{
	/** ARM9 entrypoint. If it's not set, it might still be found when parsing
	 * ELF files, if any, while building the final FIRM */
	std::optional<uint32_t> arm9_entrypoint;
	/** ARM11 entrypoint. If it's not set, it might still be found when parsing
	 * ELF files, if any, while building the final FIRM */
	std::optional<uint32_t> arm11_entrypoint;
	/** Boot priority, default should be 0 */
	uint32_t boot_priority;
	/** FIRM encryption. Default should be firm_section_encryption::NONE */
	firm_section_encryption encryption;
	/** vector of section metadata, maximum of four */
	std::vector<internal_section_header> sections;
	/** FIRM signature */
	std::array<unsigned char, 256> signature;
};

// Helper functions, pull out some functionality out of main() to make it
// easier to reason about

/** Prepares CLI argument handling.
 *
 * @returns An Options object with parameters set up for parsing argc and argv.
 */
static cxxopts::Options prepare_cli();

/** Takes in the program arguments, and converts that information into a
 * program_options structure for more structured access.
 *
 * This function does not do much checking of the values of the options, it
 * merely extracts them. check_program_options() checks consistency.
 *
 * @param[in] opts Options from cxxopts.
 * @param[in] argc Number of arguments (main argc).
 * @param[out] argv Array of string arguments (main argv).
 *
 * @returns The program options, parsed, plus all messages captured/generated
 *  while parsing. A failure is indicated by the presence of at least one
 *  log_level::FATAL message.
 */
static logged_result_type<program_options> process_opts(
	cxxopts::Options& opts, int argc, char *argv[]);

/** Checks the consistency of the program arguments, as parsed into a
 * program_options structure.
 *
 * @param[in] program_options_ Program options to check.
 *
 * @returns A log_messages container with no log_level::FATAL messages on
 *  success, any log_level::FATAL messages in the container indicates a
 *  failure.
 */
static log_messages check_program_options(
	const program_options& program_options_);

/** Based on program options/parameters, builds a JSON object of the FIRM
 * header/configuration.
 *
 * @param[in] program_options_ Program options to use to generate the FIRM JSON
 *  header.
 *
 *  @returns a logged_result_type with the FIRM JSON configuration (use
 *   get()) and any logging messages (use messages()). The presence of
 *   log_level::FATAL messages indicates a failure. The FIRM JSON configuration
 *   can be empty if the user did not request either a config file or to use
 *   standard input.
 */
static logged_result_type<nlohmann::json> process_configuration_file(
	const program_options program_options_);

/** Merges override information from the program options with the primary
 * configuration file.
 *
 * @param[in] config Primary FIRM JSON configuration. Can be an empty JSON
 *  object.
 * @param[in] program_options_ Program options with override information to
 *  merge with the primary FIRM JSON configuration. Overrides can be an empty
 *  array.
 *
 *  @returns a logged_result_type with the merged JSON configuration (use
 *   get()) and any logging messages (use messages()). The presence of
 *   log_level::FATAL messages indicates a failure.
 */
static logged_result_type<nlohmann::json> process_overrides(
	const nlohmann::json& config, const program_options& program_options_);

/** Converts a full JSON FIRM configuration object into a FIRM.
 *
 * @param[in] config The JSON representation of the FIRM configuration.
 *
 * @returns The FIRM, plus all messages captured/generated while parsing. A
 *  failure is indicated by the presence of at least one log_level::FATAL
 *  message.
 */
static logged_result_type<firm> process_config(const nlohmann::json& config);

int main(int argc, char *argv[])
{

	// Stage 1: Set up parameter handling
	auto opts = prepare_cli();

	// Stage 2: Process CLI parameters, returns program_options
	auto opts_result = process_opts(opts, argc, argv);
	auto program_options_ = opts_result.get();

	// Stage 2.5: Now check results of stage 2
	log_level error_level = program_options_.error_warnings ?
		log_level::WARN : log_level::FATAL;

	// Bail out early if the user is asking for help
	if (program_options_.help)
	{
		std::cout << opts.help() << std::endl;
		return 0;
	}
	log_messages program_options_messages =
		check_program_options(program_options_);
	// Merge messages just to make it easier to check status
	opts_result.messages().insert(opts_result.messages().end(),
		program_options_messages.begin(), program_options_messages.end());
	for (auto& msg : opts_result.messages())
		logging(msg.first) << msg.second << std::endl;
	if (opts_result.at_least(error_level))
	{
		// Print out message here, since if there's an error it's probably due
		// to a misuse of parameters
		std::cerr << opts.help() << std::endl;
		return 1;
	}

	// Stage 3: Use program_options to get config file, if there's one, and
	// update program behavior based on program_options
	logging_set_max(program_options_.verbose);
	logged_result_type<nlohmann::json> configuration_file_result =
		process_configuration_file(program_options_);

	for (auto& msg : configuration_file_result.messages())
		logging(msg.first) << msg.second << std::endl;
	if (configuration_file_result.at_least(error_level))
		return 1;

	// Stage 4:
	// Alright, at this point we're done loading from the configuration (if any)
	// merge overrides
	logged_result_type<nlohmann::json> override_result =
		process_overrides(configuration_file_result.get(), program_options_);
	for (auto& msg : override_result.messages())
		logging(msg.first) << msg.second << std::endl;
	if (override_result.at_least(error_level))
		return 1;

	// Stage 5: process JSON configuration data to build FIRM
	logging(log_level::VERBOSE) << "Parsing configuration" << std::endl;
	auto firm_result = process_config(override_result.get());
	for (auto& msg : firm_result.messages())
		logging(msg.first) << msg.second << std::endl;
	if (!program_options_.force_build && firm_result.at_least(error_level))
		return 1;
	if (program_options_.force_build)
	{
		if (firm_result.at_least(error_level))
			logging(log_level::WARN) <<
				"Errors detected with FIRM, forcing building FIRM due to force_build argument." <<
				std::endl;
	}

	// Stage 6: Write FIRM to disk
	auto& firm_ = firm_result.get();
	logging(log_level::INFO) << to_string(firm_.header);
	logging(log_level::INFO) << "Writing FIRM to " <<
		program_options_.output << std::endl;
	std::ofstream output(program_options_.output, std::ios::binary);
	output << firm_;

	return 0;
}

cxxopts::Options prepare_cli()
{
	const char *description = "Program to build 3DS FIRM images";
	cxxopts::Options opts("firmbuilder", description);
	opts.add_options()
		("c,config", "JSON configuration file describing FIRM",
		 cxxopts::value<std::vector<std::string>>())
		("o,output", "Output filename",
		 cxxopts::value<std::string>()->default_value("a.firm"))
		("f,force", "Force overwriting of existing output")
		("s,stdin", "Read from standard input instead of a file")
		("v,verbose", "Level of verbosity. Defaults to INFO if not specified, "
		 "VERBOSE if specified without an argument. Possible values are: [ "
		 "FATAL, WARN, INFO, VERBOSE, DEBUG ]",
			cxxopts::value<std::string>()->implicit_value("VERBOSE"))
		("e,error_warnings", "Make warnings into errors")
		("x,force_build", "Build FIRM even if there are erros (if possible)")
		("N,arm9_entry", "Apply override or specify new ARM9 entrypoint",
			cxxopts::value<std::string>())
		("E,arm11_entry", "Apply override or specify new ARM11 entrypoint",
			cxxopts::value<std::string>())
		("B,boot_priority", "Apply override or specify new boot priority",
			cxxopts::value<std::string>())
		("C,encryption", "Apply override or specify FIRM encryption",
			cxxopts::value<std::string>())
		("I,signature", "SIGHAX signature to use (defaults to retail NAND)",
			cxxopts::value<std::string>())
		("S,sections", "Apply overrides or specify new sections as a JSON string",
			cxxopts::value<std::string>())
		("h,help", "Print this help information")
	;
	opts.positional_help("[JSON configuration file]");

	return opts;
}

static result_type<cxxopts::ParseResult, log_messages>
	get_opts(cxxopts::Options& opts, int argc, char *argv[])
{
	// We need to do this convoluted mess because cxxopts doesn't provide a
	// default constructor for ParseResult
	opts.parse_positional("config");
	try
	{
		return result_type<cxxopts::ParseResult, log_messages>(
			opts.parse(argc, const_cast<const char**&>(argv)));
	}
	catch(cxxopts::exceptions::exception& e)
	{
		// Since most (all?) params are strings, this should pretty much almost
		// never trigger now...
		return result_type<cxxopts::ParseResult, log_messages>(
			log_messages{{log_level::FATAL, e.what()}});
	}
}

static logged_result_type<program_options>
	process_opts(cxxopts::Options& opts, int argc, char *argv[])
{
	auto opts_get_result = get_opts(opts, argc, argv);
	if (!opts_get_result)
	{
		return logged_result_type<program_options>(
			program_options{}, opts_get_result.error());
	}

	logged_result_type<program_options> result{};
	program_options &result_data = result.get();
	log_messages &messages = result.messages();

	cxxopts::ParseResult& opts_result = opts_get_result.get();

	if (opts_result.count("help"))
	{
		result_data.help = true;
	}

	result_data.verbose = log_level::INFO;
	if (opts_result.count("verbose"))
	{
		std::string verbose = opts_result["verbose"].as<std::string>();
		try
		{
			result_data.verbose = from_string<log_level>(verbose);
		}
		catch (std::invalid_argument&)
		{
			std::stringstream ss;
			ss << "Invalid verbose parameter: " << verbose;
			messages.emplace_back(log_level::FATAL, ss.str());
		}
	}
	if (opts_result.count("config"))
	{
		if (opts_result["config"].as<std::vector<std::string>>().size() == 1)
		{
			std::filesystem::path config_path =
				opts_result["config"].as<std::vector<std::string>>()[0];
			result_data.config = config_path;
		}
		else
		{
			messages.emplace_back(log_level::FATAL,
				"Too many positional parameters specified, only one accepted");
		}
	}
	result_data.standard_input = opts_result.count("stdin");

	// Default output_path based off idea of a.out in gcc
	std::filesystem::path output_path(opts_result["output"].as<std::string>());
	result_data.output = output_path;
	result_data.force = opts_result.count("force");

	result_data.error_warnings = opts_result.count("error_warnings");
	result_data.force_build = opts_result.count("force_build");

	// Now, check for overrides
	if (opts_result.count("arm9_entry"))
		result_data.overrides["arm9_entry"] =
			opts_result["arm9_entry"].as<std::string>();
	if (opts_result.count("arm11_entry"))
		result_data.overrides["arm11_entry"] =
			opts_result["arm11_entry"].as<std::string>();
	if (opts_result.count("boot_priority"))
		result_data.overrides["boot_priority"] =
			opts_result["boot_priority"].as<std::string>();
	if (opts_result.count("encryption"))
		result_data.overrides["encryption"] =
			opts_result["encryption"].as<std::string>();
	if (opts_result.count("signature"))
		result_data.overrides["signature"] =
			opts_result["signature"].as<std::string>();
	if (opts_result.count("sections"))
	{
		nlohmann::json sections_json;
		try
		{
			sections_json = nlohmann::json::parse(
				opts_result["sections"].as<std::string>());
		}
		catch (nlohmann::json::parse_error& error)
		{
			messages.emplace_back(log_level::FATAL, error.what());
		}
		result_data.overrides["sections"] = sections_json;
	}
	return result;
}

static log_messages check_program_options(
		const program_options& program_options_)
{
	log_messages result{};
	auto& messages = result;
	// Begin checking parameters
	if (program_options_.config && program_options_.standard_input)
	{
		messages.emplace_back(log_level::FATAL,
			"Can't read both from a config file and stdin, select only one");
	}
	else if (program_options_.config)
	{
		if (!std::filesystem::is_regular_file(*program_options_.config))
		{
			std::stringstream ss;
			ss << "File :" << *program_options_.config << " is not a valid file";
			messages.emplace_back(log_level::FATAL, ss.str());
		}
	}

	// Default output_path based off idea of a.out in gcc
	if (program_options_.output == "a.firm")
	{
		messages.emplace_back(log_level::VERBOSE,
			"Using default output file path \"a.firm\"");
	}

	if (std::filesystem::exists(program_options_.output))
	{
		if (!program_options_.force && std::filesystem::exists(program_options_.output))
		{
			messages.emplace_back(log_level::FATAL,
				"Output file exists, not continuing. Use -f to force overwritting it.");
		}
		else if (!std::filesystem::is_regular_file(program_options_.output))
		{
			std::stringstream ss;
			ss << "Won't write to " << program_options_.output <<
				", there exists something there that does not appear to be a regular file.";
			messages.emplace_back(log_level::FATAL, ss.str());
		}
	}

	// Now, check for overrides
	if (program_options_.overrides.is_object())
	{
		auto& overrides = program_options_.overrides;

		if (overrides.count("arm9_entry") &&
				!overrides["arm9_entry"].is_number() &&
				!overrides["arm9_entry"].is_string())
		{
			messages.emplace_back(log_level::FATAL,
				"arm9_entry override is not a number");
		}
		if (overrides.count("arm11_entry") &&
				!overrides["arm11_entry"].is_number() &&
				!overrides["arm11_entry"].is_string())
		{
			messages.emplace_back(log_level::FATAL,
				"arm11_entry override is not a number");
		}
		if (overrides.count("boot_priority") &&
				!overrides["boot_priority"].is_number() &&
				!overrides["boot_priority"].is_string())
		{
			messages.emplace_back(log_level::FATAL,
				"boot_priority override is not a number or hex string");
		}
		if (overrides.count("encryption") &&
				!overrides["encryption"].is_string())
		{
			messages.emplace_back(log_level::FATAL,
				"encryption override is not a string");
		}
		if (overrides.count("signature") &&
				!overrides["signature"].is_string())
		{
			messages.emplace_back(log_level::FATAL,
				"signature override is not a string");
		}
		if (overrides.count("sections"))
		{
			if (overrides["sections"].is_array())
			{
				for (size_t i = 0; i < overrides["sections"].size(); ++i)
				{
					auto& section = overrides["sections"][i];
					if (section.count("load_address") &&
							!section["load_address"].is_number() &&
							!section["load_address"].is_string())
					{
						std::stringstream ss;
						ss << "section " << i << " load_address is not a number or hex string";
						messages.emplace_back(log_level::FATAL, ss.str());
					}
					if (section.count("copy_method") && !section["copy_method"].is_string())
					{
						std::stringstream ss;
						ss << "section " << i << " copy_method is not a string";
						messages.emplace_back(log_level::FATAL, ss.str());
					}
					if (section.count("binary") && !section["binary"].is_string())
					{
						std::stringstream ss;
						ss << "section " << i << " binary is not a string";
						messages.emplace_back(log_level::FATAL, ss.str());
					}
				}
			}
			else
			{
				messages.emplace_back(log_level::FATAL,
					"sections override is not an array");
			}
		}
	}
	else if (!program_options_.overrides.is_null())
	{
		messages.emplace_back(log_level::FATAL,
			"BUG: Overrides internal object is not a valid JSON object");
	}

	return result;
}

logged_result_type<nlohmann::json> process_configuration_file(
		const program_options program_options_)
{
	logged_result_type<nlohmann::json> result{};
	auto& messages = result.messages();
	auto& config = result.get();

	std::filesystem::path output_path(program_options_.output);

	// Figure out where the config file is coming from
	std::ifstream config_file;
	std::filesystem::path config_path;
	if (!program_options_.standard_input && program_options_.config)
	{
		config_path = *program_options_.config;
		config_file.open(config_path);
	}

	if (program_options_.config || program_options_.standard_input)
	{
		std::stringstream ss;
		ss << "Using " <<
			(program_options_.standard_input ?
				"standard input" : config_path.string()) <<
			" as the FIRM configuration source";

		messages.emplace_back(log_level::INFO, ss.str());
	}
	else
	{
		messages.emplace_back(log_level::INFO, "Using only arguments to construct FIRM configuration");
	}

	messages.emplace_back(log_level::VERBOSE, "Reading configuration");

	// Now load the config file
	if (program_options_.config || program_options_.standard_input)
	{
		try
		{
			(program_options_.standard_input ? std::cin : config_file) >> config;
		}
		catch (nlohmann::json::parse_error& error)
		{
			messages.emplace_back(log_level::FATAL, error.what());
		}
	}
	else
	{
		config = nlohmann::json::object();
	}
	return result;
}

logged_result_type<nlohmann::json> process_overrides(
		const nlohmann::json& config, const program_options& program_options_)
{
	logged_result_type<nlohmann::json> result{};
	auto& config_ = result.get();
	config_ = config;
	auto& messages = result.messages();
	auto& overrides = program_options_.overrides;

	if (!config_.count("boot_priority"))
		config_["boot_priority"] = 0;
	handle_json_value(config_["boot_priority"], messages, overrides, "boot_priority", true);

	uint32_t entry;
	if (handle_json_value(entry, messages, overrides, "arm9_entry", true))
		config_["arm9_entry"] = entry;
	if (handle_json_value(entry, messages, overrides, "arm11_entry", true))
		config_["arm11_entry"] = entry;
	// encryption is optional, and config["encryption"] instantiates an empty
	// object even if it isn't assigned, so first check if we should
	// handle_json_value.
	if (overrides.count("encryption"))
		handle_json_value(config_["encryption"], messages, overrides, "encryption", true);
	// signature is optional, see above for rationale for checking existence
	// first
	if (overrides.count("signature"))
		handle_json_value(config_["signature"], messages, overrides, "signature", true);

	if (overrides.count("sections"))
	{
		if (overrides["sections"].is_array())
		{
			if (!config_.count("sections"))
				config_["sections"] = nlohmann::json::array();
			config_["sections"] = merge_json(config_["sections"], overrides["sections"]);
		}
		else
		{
			messages.emplace_back(log_level::FATAL, "section override is not a JSON array");
		}
	}
	return result;
}

/** Builds a final FIRM image from the program's internal FIRM state.
 *
 * @param[in] internal_firm_ Internal FIRM information to use for building the
 *  final FIRM image.
 *
 * @returns a logged_result_type with the FIRM image (use get()) and any
 *  logging messages (use messages()). The presence of log_level::FATAL
 *  messages indicates a failure.
 */
logged_result_type<firm> process_internal(const internal_firm& internal_firm_)
{
	logged_result_type<firm> result{};
	auto& firm_ = result.get();
	auto& messages = result.messages();

	memcpy(firm_.header.magic.data(), "FIRM", 4);
	firm_.header.signature = internal_firm_.signature;
	firm_.header.boot_priority = internal_firm_.boot_priority;
	std::optional<uint32_t> arm9_entrypoint = internal_firm_.arm9_entrypoint;
	std::optional<uint32_t> arm11_entrypoint = internal_firm_.arm11_entrypoint;
	for (size_t i = 0; i < internal_firm_.sections.size(); ++i)
	{
		auto& internal_header = internal_firm_.sections[i];
		firm_section_header section_header{};
		std::filesystem::path file_path(internal_header.file_path);

		section_header.copy_method = internal_header.copy_method;

		if (std::filesystem::is_regular_file(file_path))
		{
			// Try loading it as an elf first
			std::ifstream file(file_path, std::ios::binary);
			size_t file_size = std::filesystem::file_size(file_path);
			std::vector<unsigned char> binary(file_size);
			file.read(reinterpret_cast<char*>(binary.data()), file_size);
			elf_wrapper elf(binary);

			if (elf)
			{
				logging(log_level::DEBUG) << file_path <<
					" is an ELF archive" << std::endl;
				section_header.load_address = elf.get_load_address();
				binary = elf.flatten();
				if (internal_header.copy_method == firm_copy_method::NDMA &&
						!arm9_entrypoint)
				{
					arm9_entrypoint = elf.get_entrypoint();
				}
				if (internal_header.copy_method == firm_copy_method::XDMA &&
						!arm11_entrypoint)
				{
					arm11_entrypoint = elf.get_entrypoint();
				}

			}
			else if (!internal_header.load_address)
			{
				messages.emplace_back(log_level::FATAL,
					"\"section\" Missing required \"load_address\" for a non-elf binary");
			}

			if (!binary.size())
			{
				std::stringstream ss;
				ss << "binary " << file_path <<
					" provided for \"section\" produced an empty executable";
				messages.emplace_back(log_level::FATAL, ss.str());
			}

			if (internal_header.load_address)
			{
				if (elf)
				{
					std::stringstream ss;
					ss << "Overriding load address for " << file_path <<
						" with " << section_header.load_address;
					messages.emplace_back(log_level::VERBOSE, ss.str());
				}
				section_header.load_address = *internal_header.load_address;
			}

			// Alright, we have to pad it to a multiple of sector size (512)
			binary.resize(((binary.size() + 511)/512) * 512, 0xFFu);
			section_header.sha256 = sha256sum(binary);
			section_header.size = binary.size();
			firm_.sections.emplace_back(std::move(binary));
		}
		else
		{
			std::stringstream ss;
			ss << "File " << file_path << " is not a regular file";
			messages.emplace_back(log_level::FATAL, ss.str());
		}

		// Calculate offset for section
		if (i == 0)
		{
			section_header.offset = 0x200;
		}
		else
		{
			auto& prev = firm_.header.section_headers[i-1];
			section_header.offset = prev.offset + prev.size;
		}
		firm_.header.section_headers.emplace_back(std::move(section_header));

		if (internal_firm_.encryption != firm_section_encryption::NONE)
		{
			auto& section = firm_.sections.back();
			section = encrypt_section(
				section_header, section, internal_firm_.encryption);
		}

	}

	if (arm9_entrypoint)
		firm_.header.arm9_entrypoint = *arm9_entrypoint;
	else
		messages.emplace_back(log_level::FATAL, "arm9_entrypoint is missing");
	if (arm11_entrypoint)
		firm_.header.arm11_entrypoint = *arm11_entrypoint;
	else
		messages.emplace_back(log_level::FATAL, "arm11_entrypoint is missing");

	return result;
}

/** Parses out section header information from a JSON object.
 *
 * @param[in] sections_json JSON array with section header information.
 *
 * @returns a logged_result_type with the section header information (use
 *  get()) and any logging messages (use messages()). The presence of
 *  log_level::FATAL messages indicates a failure.
 */
logged_result_type<std::vector<internal_section_header>> parse_sections(
	const nlohmann::json& sections_json)
{
	logged_result_type<std::vector<internal_section_header>> result;
	std::vector<internal_section_header> &sections = result.get();
	log_messages &messages = result.messages();

	if (!sections_json.is_array())
	{
		messages.emplace_back(log_level::FATAL,
			"sections entry is not a JSON array");
		return result;
	}

	for (size_t i = 0; i < sections_json.size(); ++i)
	{
		auto& section_json = sections_json[i];
		internal_section_header section{};

		if (!section_json.is_object())
		{
			messages.emplace_back(
				log_level::FATAL, "\"section\" is not a JSON objects");
		}

		std::string copy_method_string;
		handle_json_value(
			copy_method_string, messages, section_json, "copy_method");

		if (section_json.count("load_address"))
		{
			uint32_t load_address;
			if (handle_json_value(
					load_address, messages, section_json, "load_address"))
				section.load_address = load_address;
		}

		try
		{
			section.copy_method =
				from_string<firm_copy_method>(copy_method_string);
		}
		catch (std::invalid_argument&)
		{
			std::stringstream ss;
			ss << "Unknown copy method \"" << copy_method_string << "\" found";
			messages.emplace_back(log_level::FATAL, ss.str());
		}

		std::string binary_string;
		handle_json_value(binary_string, messages, section_json, "binary");

		section.file_path = binary_string;
		sections.emplace_back(std::move(section));
	}
	return result;
}

/** Parses out FIRM header (and section header) information from a JSON object.
 *
 * @param[in] header_json JSON array with FIRM and section header information.
 *
 * @returns a logged_result_type with the FIRM and section header information
 *  (use get()) and any logging messages (use messages()). The presence of
 *  log_level::FATAL messages indicates a failure.
 */
logged_result_type<internal_firm> parse_header(
		const nlohmann::json& header_json)
{
	logged_result_type<internal_firm> result;
	internal_firm& firm_ = result.get();
	log_messages& messages = result.messages();

	// Generally speaking, we don't care if things fail, as they're being
	// stored as failures (FATAL) in the logged_result_type

	// Boot priority is optional, so set a default value to begin with
	firm_.boot_priority = 0;
	if (header_json.contains("boot_priority"))
	{
		handle_json_value(
			firm_.boot_priority, messages, header_json, "boot_priority");
	}
	else
	{
		messages.emplace_back(
			log_level::VERBOSE, "Setting boot priority to default 0");
	}

	// Both arm9 and arm11 entries are optional, pending sections being
	// processed
	if (header_json.count("arm9_entry"))
	{
		uint32_t entry = 0;
		handle_json_value(
			entry, messages, header_json, "arm9_entry", true);
		firm_.arm9_entrypoint = entry;
	}
	if (header_json.count("arm11_entry"))
	{
		uint32_t entry = 0;
		handle_json_value(
			entry, messages, header_json, "arm11_entry", true);
		firm_.arm11_entrypoint = entry;
	}
	if (firm_.arm11_entrypoint == 0)
	{
		messages.emplace_back(log_level::WARN,
			"3DS boot9 won't load this FIRM, ARM11 entry is 0x0");
	}

	firm_.encryption = firm_section_encryption::NONE;
	if (header_json.contains("encryption"))
	{
		if (header_json["encryption"].is_string())
		{
			std::string encryption;
			handle_json_value(encryption, messages, header_json, "encryption");
			try
			{
				firm_.encryption =
					from_string<firm_section_encryption>(encryption);
			}
			catch (std::invalid_argument&)
			{
				messages.emplace_back(log_level::FATAL,
					"Invalid encryption requested: " + encryption);
			}
		}
		else
		{
			messages.emplace_back(log_level::FATAL,
				"encryption entry is of the wrong type");
		}
	}

	if (header_json.contains("signature"))
	{
		sighax_signature signature;
		std::string sig_str;
		handle_json_value(
			sig_str, messages, header_json, "signature", true);
		try
		{
			signature =
				from_string<sighax_signature>(sig_str);
		}
		catch (std::invalid_argument&)
		{
			messages.emplace_back(log_level::FATAL,
				"Invalid signature requested: " + sig_str);
		}
		firm_.signature = *sighax_signatures[static_cast<int>(signature)];
	}
	else
	{
		firm_.signature = retail_nand_firm_sig;
	}

	// Sections is a required field, as there must be at least one section
	if (header_json.contains("sections"))
	{
		if (!header_json["sections"].is_array() ||
				header_json["sections"].empty())
		{
			messages.emplace_back(
				log_level::FATAL, "Sections entry is not an array or is empty");
		}
		else if (header_json["sections"].size() > 4)
		{
			messages.emplace_back(
				log_level::FATAL, "There are too many sections");
		}
		else
		{
			auto parsed_sections = parse_sections(header_json["sections"]);
			firm_.sections = parsed_sections.get();
			messages.insert(messages.end(),
					parsed_sections.messages().begin(),
					parsed_sections.messages().end());
		}
	}
	else
	{
		messages.emplace_back(
			log_level::FATAL, "Missing required entry \"sections\"");
	}
	return result;
}

static logged_result_type<firm> process_config(const nlohmann::json& config)
{
	// Begin preparing FIRM data structures
	logged_result_type<firm> result;
	firm &firm_ = result.get();
	log_messages &messages = result.messages();

	auto parsed_header = parse_header(config);
	internal_firm internal_firm_ = parsed_header.get();
	messages = parsed_header.messages();

	auto processed_firm = process_internal(internal_firm_);
	firm_ = processed_firm.get();
	messages.insert(messages.end(),
		processed_firm.messages().begin(),
		processed_firm.messages().end());

	// file/data. Make sure the firm we have prepared is valid one final time
	messages.emplace_back(log_level::VERBOSE,
		"Finished parsing configuration file. Checking for consistency");
	log_messages check_result = check_firm(firm_, internal_firm_.encryption);
	messages.insert(messages.end(), check_result.begin(), check_result.end());

	return result;
}

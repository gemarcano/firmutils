// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020

#include <firmutils/firmutils.h>
#include <firmutils/firm.h>
#include <firmutils/crypto.h>
#include <firmutils/log.h>

#include <iostream>
#include <array>
#include <iomanip>
#include <ostream>
#include <istream>
#include <vector>
#include <cstring>
#include <utility>

extern "C" {
	firmutils_encryption firmutils_encryption_from_string(const char *str)
	{
		try
		{
			return static_cast<firmutils_encryption>(firmutils::from_string<firmutils::firm_section_encryption>(str));
		}
		catch (std::invalid_argument&)
		{
			return FIRMUTILS_ENC_NONE;
		}
	}

	const char* firmutils_encryption_to_string(firmutils_encryption encryption)
	{
		return firmutils::to_string(static_cast<firmutils::firm_section_encryption>(encryption)).data();
	}

	firmutils_copy_method firmutils_copy_method_from_string(const char *str)
	{
		try
		{
			return static_cast<firmutils_copy_method>(firmutils::from_string<firmutils::firm_copy_method>(str));
		}
		catch (std::invalid_argument&)
		{
			return FIRMUTILS_COPY_NDMA;
		}
	}

	const char* firmutils_copy_method_to_string(firmutils_copy_method method)
	{
		return firmutils::to_string(static_cast<firmutils::firm_copy_method>(method)).data();
	}

	firmutils_firm *firmutils_firm_new(void)
	{
		return reinterpret_cast<firmutils_firm*>(new firmutils::firm{});
	}

	void firmutils_firm_free(firmutils_firm *firm)
	{
		if (firm)
			delete reinterpret_cast<firmutils::firm*>(firm);
	}

	firmutils_header* firmutils_firm_get_header(firmutils_firm *firm)
	{
		return reinterpret_cast<firmutils_header*>(&reinterpret_cast<firmutils::firm*>(firm)->header);
	}

	void firmutils_firm_set_header(firmutils_firm *firm, const firmutils_header *header)
	{
		reinterpret_cast<firmutils::firm*>(firm)->header = *reinterpret_cast<const firmutils::firm_header*>(header);
	}

	void firmutils_firm_get_section_data(firmutils_firm *firm, size_t index,  unsigned char **data, size_t *size)
	{
		auto& firm_ = *reinterpret_cast<firmutils::firm*>(firm);
		for (size_t i = firm_.sections.size(); i <= index; ++i)
			firm_.sections.emplace_back();

		if (index < 4)
		{
			*data = firm_.sections[index].data();
			*size = firm_.sections[index].size();
		}
		else
		{
			*data = nullptr;
			*size = 0;
		}
	}

	void firmutils_firm_set_section_data(firmutils_firm *firm, size_t index, unsigned char *data, size_t size)
	{
		auto& firm_ = *reinterpret_cast<firmutils::firm*>(firm);
		for (size_t i = firm_.sections.size(); i <= index; ++i)
			firm_.sections.emplace_back();
		firm_.sections[index].assign(data, data+size);
	}

	firmutils_header *firmutils_header_new(void)
	{
		return reinterpret_cast<firmutils_header*>(new firmutils::firm_header{});
	}

	void firmutils_header_free(firmutils_header *header)
	{
		if (header)
			delete reinterpret_cast<firmutils::firm_header*>(header);
	}

	const unsigned char* firmutils_header_get_magic(const firmutils_header *header)
	{
		return reinterpret_cast<const firmutils::firm_header*>(header)->magic.data();
	}

	uint32_t firmutils_header_get_boot_priority(const firmutils_header *header)
	{
		return reinterpret_cast<const firmutils::firm_header*>(header)->boot_priority;
	}

	uint32_t firmutils_header_get_arm11_entrypoint(const firmutils_header *header)
	{
		return reinterpret_cast<const firmutils::firm_header*>(header)->arm11_entrypoint;
	}

	uint32_t firmutils_header_get_arm9_entrypoint(const firmutils_header *header)
	{
		return reinterpret_cast<const firmutils::firm_header*>(header)->arm9_entrypoint;
	}

	const unsigned char* firmutils_header_get_signature(const firmutils_header *header)
	{
		return reinterpret_cast<const firmutils::firm_header*>(header)->signature.data();
	}

	void firmutils_header_set_magic(firmutils_header *header, const unsigned char *magic)
	{
		memcpy(reinterpret_cast<firmutils::firm_header*>(header)->magic.data(), magic, 4);
	}

	void firmutils_header_set_boot_priority(firmutils_header *header, uint32_t boot_priority)
	{
		reinterpret_cast<firmutils::firm_header*>(header)->boot_priority = boot_priority;
	}

	void firmutils_header_set_arm11_entrypoint(firmutils_header *header, uint32_t arm11_entrypoint)
	{
		reinterpret_cast<firmutils::firm_header*>(header)->arm11_entrypoint = arm11_entrypoint;
	}

	void firmutils_header_set_arm9_entrypoint(firmutils_header *header, uint32_t arm9_entrypoint)
	{
		reinterpret_cast<firmutils::firm_header*>(header)->arm9_entrypoint = arm9_entrypoint;
	}

	void firmutils_header_set_signature(firmutils_header *header, const unsigned char *signature)
	{
		memcpy(reinterpret_cast<firmutils::firm_header*>(header)->signature.data(), signature, 256);
	}

	void firmutils_header_set_section_header(firmutils_header *header, size_t index, const firmutils_section_header *section_header)
	{
		auto& header_ = *reinterpret_cast<firmutils::firm_header*>(header);
		for (size_t i = header_.section_headers.size(); i <= index; ++i)
		{
			header_.section_headers.emplace_back();
		}
		header_.section_headers[index] = *reinterpret_cast<const firmutils::firm_section_header*>(section_header);
	}

	firmutils_section_header* firmutils_header_get_section_header(firmutils_header *header, size_t index)
	{
		auto& header_ = *reinterpret_cast<firmutils::firm_header*>(header);
		if (index > 3)
			return nullptr;
		for (size_t i = header_.section_headers.size(); i <= index; ++i)
			header_.section_headers.emplace_back();

		return reinterpret_cast<firmutils_section_header*>(&header_.section_headers[index]);
	}

	firmutils_section_header* firmutils_section_header_new(void)
	{
		return reinterpret_cast<firmutils_section_header*>(new firmutils::firm_section_header{});
	}

	void firmutils_section_header_free(firmutils_section_header *header)
	{
		if (header)
			delete reinterpret_cast<firmutils::firm_section_header*>(header);
	}

	uint32_t firmutils_section_header_get_offset(const firmutils_section_header *header)
	{
		return reinterpret_cast<const firmutils::firm_section_header*>(header)->offset;
	}

	uint32_t firmutils_section_header_get_load_address(const firmutils_section_header *header)
	{
		return reinterpret_cast<const firmutils::firm_section_header*>(header)->load_address;
	}

	uint32_t firmutils_section_header_get_size(const firmutils_section_header *header)
	{
		return reinterpret_cast<const firmutils::firm_section_header*>(header)->size;
	}

	firmutils_copy_method firmutils_section_header_get_copy_method(const firmutils_section_header *header)
	{
		return static_cast<firmutils_copy_method>(reinterpret_cast<const firmutils::firm_section_header*>(header)->copy_method);
	}

	const unsigned char* firmutils_section_header_get_sha256(const firmutils_section_header *header)
	{
		return reinterpret_cast<const firmutils::firm_section_header*>(header)->sha256.data();
	}

	void firmutils_section_header_set_offset(firmutils_section_header *header, uint32_t offset)
	{
		reinterpret_cast<firmutils::firm_section_header*>(header)->offset = offset;
	}

	void firmutils_section_header_set_load_address(firmutils_section_header *header, uint32_t load_address)
	{
		reinterpret_cast<firmutils::firm_section_header*>(header)->load_address = load_address;
	}

	void firmutils_section_header_set_size(firmutils_section_header *header, uint32_t size)
	{
		reinterpret_cast<firmutils::firm_section_header*>(header)->size = size;
	}

	void firmutils_section_header_set_copy_method(firmutils_section_header *header, firmutils_copy_method method)
	{
		reinterpret_cast<firmutils::firm_section_header*>(header)->copy_method = static_cast<firmutils::firm_copy_method>(method);
	}

	void firmutils_section_header_set_sha256(firmutils_section_header *header, const unsigned char *sha256)
	{
		memcpy(reinterpret_cast<firmutils::firm_section_header*>(header)->sha256.data(), sha256, 32);
	}

	size_t firmutils_firm_header_to_string(const firmutils_header *header, unsigned char *buf, size_t buf_size)
	{
		auto header_ = reinterpret_cast<const firmutils::firm_header*>(header);
		std::string str = firmutils::to_string(*header_);
		if (!buf)
			return str.length();
		size_t result = std::min(buf_size - 1 , str.length());
		memcpy(buf, str.data(), result);
		buf[result] = 0;
		return result;
	}

	size_t firmutils_build_firm(const firmutils_firm *firm, unsigned char *buf, size_t buf_size)
	{
		auto firm_ = reinterpret_cast<const firmutils::firm*>(firm);
		std::stringstream ss;
		ss << *firm_;
		if (!buf)
			return ss.str().length();
		size_t result = std::min(buf_size, ss.str().length());
		memcpy(buf, ss.str().data(), result);
		return result;
	}

	size_t firmutils_parse_firm(firmutils_firm *firm, const unsigned char *buf, size_t buf_size)
	{
		auto firm_ = reinterpret_cast<firmutils::firm*>(firm);
		std::stringstream ss;
		ss.write(reinterpret_cast<const char*>(buf), buf_size);
		ss >> *firm_;
		return buf_size;
	}

	size_t firmutils_encrypt_section(const firmutils_section_header *header, const unsigned char *src, size_t src_size, unsigned char *dst, size_t dest_size, firmutils_encryption encryption)
	{
		auto header_ = reinterpret_cast<const firmutils::firm_section_header*>(header);
		std::vector<unsigned char> data(src, src+src_size);
		auto encrypted = firmutils::encrypt_section(*header_, data, static_cast<firmutils::firm_section_encryption>(encryption));
		if (!dst)
		{
			return encrypted.size();
		}
		size_t result = std::min(dest_size, encrypted.size());
		memcpy(dst, encrypted.data(), result);
		return result;
	}

	size_t firmutils_decrypt_section(const firmutils_section_header *header, const unsigned char *src, size_t src_size, unsigned char *dst, size_t dest_size, firmutils_encryption encryption)
	{
		auto header_ = reinterpret_cast<const firmutils::firm_section_header*>(header);
		std::vector<unsigned char> data(src, src+src_size);
		auto decrypted = firmutils::decrypt_section(*header_, data, static_cast<firmutils::firm_section_encryption>(encryption));
		if (!dst)
		{
			return decrypted.size();
		}
		size_t result = std::min(dest_size, decrypted.size());
		memcpy(dst, decrypted.data(), result);
		return result;
	}

	bool firmutils_check_firm(const firmutils_firm *firm, firmutils_encryption encryption)
	{
		auto firm_ = reinterpret_cast<const firmutils::firm*>(firm);
		auto result = firmutils::check_firm(*firm_, static_cast<firmutils::firm_section_encryption>(encryption));
		return result.empty();
	}
} // extern "C"

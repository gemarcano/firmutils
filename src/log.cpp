// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020

#include <firmutils/log.h>

firmutils::log_class firmutils::log_object(firmutils::log_level::INFO);

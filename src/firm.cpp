// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020

#include <firmutils/firm.h>
#include <firmutils/crypto.h>
#include <firmutils/log.h>

#include <iostream>
#include <array>
#include <iomanip>
#include <ostream>
#include <istream>
#include <vector>
#include <cstring>
#include <utility>
#include <optional>

namespace firmutils
{
	/** Helper array, index SIGHAX signatures */
	const std::array<const std::array<unsigned char, 256>*, 6>
	sighax_signatures {
		&retail_nand_firm_sig, &retail_nand_ncsd_header_sig,
		&retail_spi_flash_ntr_firm_sig, &dev_nand_firm_sig,
		&dev_nand_ncsd_header_sig, &dev_spi_flash_ntr_firm_sig};

	/** Helper, SIGHAX descriptions for previous signatures */
	static const std::array<const char*,6> sighax_signatures_names {
		"sighax retail NAND FIRM", "sighax retail NAND NCSD header",
		"sighax retail SPI Flash NTR FIRM",	"sighax dev NAND FIRM",
		"sighax dev NAND NCSD header", "sighax dev SPI Flash NTR FIRM"};

	/** boot9 blacklist sections */
	std::array<std::pair<uint32_t, uint32_t>, 7> boot9_firm_blacklist {{
		{0x07FFB800, 0x07FFFBFF},
		{0xFFF00000, 0xFFF02FFF},
		{0xFFF03000, 0xFFF03FFF},
		{0x080F8000, 0x080FFFFF},
		{0x08000000, 0x0800003F},
		{0x20000000, 0x27FFFFFF},
		{0xFFF00000, 0x20000000}, // WTF?
	}};

	enum class memory_mode
	{
		NONE = 0,
		R = 1,
		W = 2,
		RW = 3,
		X = 4,
		RX = 5,
		WX = 6,
		RWX = 7
	};

	memory_mode operator|(memory_mode a, memory_mode b)
	{
		using und_type = std::underlying_type_t<memory_mode>;
		return static_cast<memory_mode>(
			static_cast<und_type>(a) | static_cast<und_type>(b));
	}

	memory_mode operator&(memory_mode a, memory_mode b)
	{
		using und_type = std::underlying_type_t<memory_mode>;
		return static_cast<memory_mode>(
			static_cast<und_type>(a) & static_cast<und_type>(b));
	}

	struct memory_region
	{
		std::pair<uint32_t, uint32_t> region;
		memory_mode mode;
	};

	constexpr const
	std::array<memory_region, 12> arm11_physical_memory{{
		{{0x00000000, 0x0000FFFF}, memory_mode::RX}, // bootrom
		{{0x00010000, 0x0001FFFF}, memory_mode::RX}, // bootrom mirror
		{{0x10000000, 0x1040FFFF}, memory_mode::RW}, // IO, Estimated end
		{{0x17E00000, 0x17E01FFF}, memory_mode::RWX}, // MPCore private
		{{0x1E710000, 0x17E10FFF}, memory_mode::RW}, // L2C-310 level 2 cache controller
		{{0x18000000, 0x185FFFFF}, memory_mode::RWX}, // VRAM
		{{0x1F000000, 0x1F3FFFFF}, memory_mode::RWX}, // N3DS additional memory
		{{0x1FF00000, 0x1FF7FFFF}, memory_mode::RWX}, // DSP memory
		{{0x1FF80000, 0x1FFFFFFF}, memory_mode::RWX}, // AXI WRAM
		{{0x20000000, 0x27FFFFFF}, memory_mode::RWX}, // FCRAM
		{{0x28000000, 0x2FFFFFFF}, memory_mode::RWX}, // N3DS FCRAM extension
		{{0xFFFF0000, 0xFFFFFFFF}, memory_mode::RX} // bootrom mirror
	}};

	constexpr const
	std::array<memory_region, 11> arm9_physical_memory{{
		{{0x00000000, 0x07FFFFFF}, memory_mode::RWX}, // ITCM
		{{0x08000000, 0x080FFFFF}, memory_mode::RWX}, // ARM9 internal
		{{0x08100000, 0x0817FFFF}, memory_mode::RWX}, // N3DS ARM9 internal
		{{0x10000000, 0x17FFFFFF}, memory_mode::RW}, // IO
		{{0x18000000, 0x185FFFFF}, memory_mode::RWX}, // VRAM
		{{0x1FF00000, 0x1FF7FFFF}, memory_mode::RWX}, // DSP
		{{0x1FF80000, 0x1FFFFFFF}, memory_mode::RWX}, // AXI WRAM
		{{0x20000000, 0x27FFFFFF}, memory_mode::RWX}, // FCRAM
		{{0x28000000, 0x2FFFFFFF}, memory_mode::RWX}, // N3DS FCRAM
		{{0xFFF00000, 0xFFF03FFF}, memory_mode::RW}, // DTCM
		{{0xFFFF0000, 0xFFFFFFFF}, memory_mode::RX} // bootrom
	}};

	template<class Arr>
	bool memory_is_entry_valid(Arr& memory, uint32_t entry)
	{
		// This thing better be 4 byte aligned...
		if (entry % 4)
			return false;

		bool valid_region = false;
		for (auto [region, mode] : memory)
		{
			if ((mode & memory_mode::RX) != memory_mode::RX)
				continue;

			if (region.first <= entry && entry <= region.second)
			{
				valid_region = true;
				break;
			}
		}
		return valid_region;
	}

	template<class Arr>
	bool memory_is_loadable(Arr& memory, uint32_t load_address, uint32_t size)
	{
		bool valid_load = false;
		bool valid_end = false;
		size_t load_region{};
		size_t end_region{};
		uint32_t endpoint = load_address + size;
		for (size_t i = 0; i < memory.size(); ++i)
		{
			auto& [region, mode] = memory[i];
			if ((mode & memory_mode::W) != memory_mode::W)
				continue;

			if (region.first <= load_address && load_address <= region.second)
			{
				valid_load = true;
				load_region = i;
			}

			if (region.first <= endpoint && endpoint <= region.second)
			{
				valid_end = true;
				end_region = i;
			}

			if (valid_load && valid_end)
				break;
		}

		if (valid_load && valid_end)
		{
			if (load_region == end_region)
				return true;

			// OK, now the hard part-- we need to make sure the endpoint lies
			// in an area of memory that's continuous from the load address,
			// and that all areas in between are writable.
			for (size_t i = load_region + 1; i <= end_region; ++i)
			{
				auto& [region1, mode1] = memory[i-1];
				auto& [region2, mode2] = memory[i];
				// All regions in between must be writable
				if ((mode1 & memory_mode::W) != memory_mode::W || (mode2 & memory_mode::W) != memory_mode::W)
					return false;
				// Check for region continuity. If not, bail
				if (region1.second + 1 != region2.first)
					return false;
			}
			// If we made it this far, regions are continuous and writable
			return true;
		}

		return false;
	}

	firm_section_header parse_section_header(std::istream& is)
	{
		firm_section_header result{};

		auto little_endian_extract = [&]{
			uint32_t result = 0;
			unsigned char buffer[4]{};
			if (is.read(reinterpret_cast<char*>(buffer), 4))
				for (size_t i = 0; i < 4; ++i)
					result |= buffer[i] << (8*i);
			return result;
		};
		result.offset = little_endian_extract();
		result.load_address = little_endian_extract();
		result.size = little_endian_extract();
		result.copy_method =
			static_cast<firm_copy_method>(little_endian_extract());
		is.read(reinterpret_cast<char*>(result.sha256.data()), 32);

		return result;
	}

	firm_header parse_firm_header(std::istream& is)
	{
		firm_header result{};
		auto little_endian_extract = [&]{
			uint32_t result = 0;
			unsigned char buffer[4]{};
			if (is.read(reinterpret_cast<char*>(buffer), 4))
				for (size_t i = 0; i < 4; ++i)
					result |= buffer[i] << (8*i);
			return result;
		};

		is.read(reinterpret_cast<char*>(result.magic.data()), 4);
		if (!is)
			return result;
		result.boot_priority = little_endian_extract();
		result.arm11_entrypoint = little_endian_extract();
		result.arm9_entrypoint = little_endian_extract();
		is.seekg(0x30, std::ios::cur);
		for (size_t i = 0; is && i < 4; ++i)
		{
			auto section_header = parse_section_header(is);
			if (section_header.offset == 0)
				continue;
			result.section_headers.emplace_back(std::move(section_header));
		}
		is.read(reinterpret_cast<char*>(result.signature.data()), 256);
		return result;
	}

	firm parse_firm(std::istream& is)
	{
		firm_header header{};
		std::vector<std::vector<unsigned char>> sections{};
		header = parse_firm_header(is);

		for (size_t i = 0; is && i < header.section_headers.size(); ++i)
		{
			const auto& section_header = header.section_headers[i];
			sections.emplace_back(section_header.size);
			is.seekg(section_header.offset, std::ios::beg);
			is.read(reinterpret_cast<char*>(sections[i].data()),
				section_header.size);
		}
		return {header, sections};
	}

	template<class Num>
	static void write_stream_le(std::ostream& os, Num number)
	{
		for (size_t i = 0; os && i < sizeof(number); ++i)
		{
			unsigned char buf;
			buf = number >> (8*i);
			os.write(reinterpret_cast<char*>(&buf), 1);
		}
	}

	template<class Num>
	void write_buffer_le(unsigned char *buffer, Num data)
	{
		for (size_t i = 0; i < sizeof(data); ++i)
		{
			*(buffer++) = data >> (8*i);
		}
	}

	std::ostream& operator<<(std::ostream& os, const firm& firm_)
	{
		auto beg = os.tellp();
		auto& firm_header = firm_.header;
		os.write(reinterpret_cast<const char*>(firm_header.magic.data()), 4);
		write_stream_le(os, firm_header.boot_priority);
		write_stream_le(os, firm_header.arm11_entrypoint);
		write_stream_le(os, firm_header.arm9_entrypoint);
		// reserved 0x30 bytes
		std::fill_n(std::ostreambuf_iterator<char>(os), 0x30, '\0');
		for (size_t i = 0; os && i < firm_header.section_headers.size(); ++i)
		{
			auto& section = firm_header.section_headers[i];
			write_stream_le(os, section.offset);
			write_stream_le(os, section.load_address);
			write_stream_le(os, section.size);
			uint32_t method = static_cast<uint32_t>(section.copy_method);
			write_stream_le(os, method);
			os.write(
				reinterpret_cast<const char*>(section.sha256.data()), 0x20);
		}
		for (size_t i = firm_header.section_headers.size(); i < 4; ++i)
		{
			// Fill in section headers that aren't specified with all 0s
			std::fill_n(std::ostreambuf_iterator<char>(os), 0x30, '\0');
		}

		os.write(reinterpret_cast<const char*>(
				firm_header.signature.data()), 0x100);

		auto& sections = firm_.sections;
		for (size_t i = 0; os && i < sections.size(); ++i)
		{
			auto& section = firm_header.section_headers[i];
			os.seekp(beg, std::ios::beg);
			os.seekp(section.offset, std::ios::cur);
			os.write(reinterpret_cast<const char*>(sections[i].data()),
				sections[i].size());
		}

		return os;
	}

	std::istream& operator>>(std::istream& is, firm& firm)
	{
		firm = parse_firm(is);
		return is;
	}

	std::vector<unsigned char> encrypt_section(
			const firm_section_header& header,
			const std::vector<unsigned char>& data,
			firm_section_encryption key_type)
	{
		const std::array<unsigned char, 16> *key;
		switch (key_type)
		{
			case firm_section_encryption::RETAIL_SPI:
				key = &retail_spi_key; // 16 bytes
				break;
			case firm_section_encryption::DEV_SPI:
				key = &dev_spi_key;
				break;
			default:
				return std::vector<unsigned char>();
		}
		std::array<unsigned char, 16> iv;
		write_buffer_le(iv.data(), header.offset);
		write_buffer_le(iv.data()+4, header.load_address);
		write_buffer_le(iv.data()+8, header.size);
		write_buffer_le(iv.data()+12, header.size);

		return cbc128_encrypt(*key, iv, data);;
	}

	std::vector<unsigned char> decrypt_section(
			const firm_section_header& header,
			const std::vector<unsigned char>& data,
			firm_section_encryption key_type)
	{
		const std::array<unsigned char, 16> *key;
		switch (key_type)
		{
			case firm_section_encryption::RETAIL_SPI:
				key = &retail_spi_key; // 16 bytes
				break;
			case firm_section_encryption::DEV_SPI:
				key = &dev_spi_key;
				break;
			default:
				return std::vector<unsigned char>();
		}

		std::array<unsigned char, 16> iv;
		write_buffer_le(iv.data(), header.offset);
		write_buffer_le(iv.data()+4, header.load_address);
		write_buffer_le(iv.data()+8, header.size);
		write_buffer_le(iv.data()+12, header.size);

		return cbc128_decrypt(*key, iv, data);;
	}

	log_messages check_firm(
			const firm& firm_, firm_section_encryption encryption)
	{
		log_messages result{};
		auto& header = firm_.header;
		auto& sections = firm_.sections;

		// Do sanity check on header
		if (memcmp(header.magic.data(), "FIRM", 4))
			result.emplace_back(log_level::FATAL, "FIRM magic check failed");
		if (header.arm11_entrypoint == 0)
			result.emplace_back(log_level::WARN,
				"3DS boot9 won't load this FIRM, ARM11 entry is 0x0");

		// Check that arm11 and arm9 entrypoints are within valid memory regions
		if (!memory_is_entry_valid(arm11_physical_memory, header.arm11_entrypoint))
			result.emplace_back(log_level::FATAL,
				"ARM11 entrypoint is not valid (either not aligned, or not within an ARM11 valid memory region)");
		if (!memory_is_entry_valid(arm9_physical_memory, header.arm9_entrypoint))
			result.emplace_back(log_level::FATAL,
				"ARM9 entrypoint is not valid (either not aligned, or not within an ARM9 valid memory region)");

		// Check that we have the same number of sections as section_headers
		if (firm_.sections.size() != firm_.header.section_headers.size())
		{
			result.emplace_back(log_level::FATAL,
				"Number of sections and section headers do not match");
		}
		else
		{
			for (size_t i = 0; i < header.section_headers.size(); ++i)
			{
				auto& section_header = header.section_headers[i];
				auto& section = sections[i];

				// Sanity check expected size
				if (section_header.size != section.size())
				{
					result.emplace_back(log_level::FATAL,
						"Recorded section size does not match actual size");
					continue; // Don't bother doing anything else
				}

				// Check blacklisted zones
				uint32_t begin = section_header.load_address;
				uint32_t end = begin + section_header.size;

				auto check_blacklist = [begin, end]
						(const std::pair<uint32_t, uint32_t>& region)
				{
					return
						((region.first <= begin && begin <= region.second) ||
							(region.first <= end && end <= region.second));
				};
				if (std::any_of(boot9_firm_blacklist.begin(),
							boot9_firm_blacklist.end(), check_blacklist))
					result.emplace_back(log_level::FATAL,
							"section starts or ends in blacklisted zone");

				// Check that load address is valid and that all of the section
				// fits All memory operations appear to take place in ARM9
				// address space?
				if (!memory_is_loadable(
						arm9_physical_memory,
						section_header.load_address,
						section_header.size))
				{
					std::stringstream ss;
					ss << "section " << i <<
						" load address is invalid or section won't fit in memory";
					result.emplace_back(log_level::FATAL, ss.str());
				}

				// FIXME do we WANT to enforce this?
				// Check that the current offset equals the offset of the
				// previous section plus the previous section's size
				if (i == 0)
				{
					// Special case, first section, as there's no previous
					// section
					if (section_header.offset != 0x200)
						result.emplace_back(log_level::FATAL,
							"Bad section offset value");
				}
				else
				{
					auto& prev = header.section_headers[i-1];
					if (section_header.offset != prev.offset + prev.size)
						result.emplace_back(log_level::FATAL,
								"Bad section offset value");
				}

				// Check hash
				std::vector<unsigned char> section_data = section;
				if (encryption != firm_section_encryption::NONE)
					section_data = decrypt_section(
						section_header, section_data, encryption);

				if (section_header.sha256 != sha256sum(section_data))
				{
					std::stringstream ss;
					ss << "section " << i <<
						" sha256 does not match section_header sha256";
					result.emplace_back(log_level::FATAL, ss.str());
				}

				// We don't really have a way to check copy_method, and as it's
				// an enum, anything that's not valid is undefined behavior
			}
		}

		bool found = false;
		for (size_t i = 0; i < sighax_signatures.size() && !found; ++i)
		{
			if (header.signature == *sighax_signatures[i])
			{
				std::stringstream ss;
				ss << "Using " << sighax_signatures_names[i] <<
					" as the FIRM header signature";
				result.emplace_back(log_level::VERBOSE, ss.str());
				found = true;
			}
		}
		if (!found)
			result.emplace_back(log_level::WARN,
				"Using an unknown FIRM header signature");

		// FIXME do we want to enforce a size limit?
		return result;
	}

	std::string to_string(const firm_header& header)
	{
		std::stringstream ss;
		ss << "    magic: " <<
			std::string_view(
				reinterpret_cast<const char*>(header.magic.data()), 4) << "\n";
		ss << "    boot priority: " << header.boot_priority << "\n";
		ss << "    arm11 entrypoint: " << std::uppercase << std::hex <<
			"0x" << header.arm11_entrypoint << "\n";
		ss << "    arm9 entrypoint: " << std::hex <<
			"0x" << header.arm9_entrypoint << "\n";
		ss << "    signature: " << std::hex;
		static_assert(sizeof(header.signature) == 256);
		for (size_t i = 0; i < header.signature.size(); ++i)
		{
			if (i % 32 == 0)
				ss << "\n        ";
			ss << std::hex << std::setfill('0') << std::setw(2) <<
				static_cast<unsigned>(header.signature[i]);
		}
		ss << "\n";

		for (size_t i = 0; i < header.section_headers.size(); ++i)
		{
			auto& section_header = header.section_headers[i];
			ss << "    section " << std::dec << i << ":\n";
			ss << "        byte offset: " << section_header.offset << "\n";
			ss << "        load address: " << "0x" << std::hex <<
				section_header.load_address << "\n";
			ss << "        size: " << std::dec << section_header.size << "\n";
			ss << "        copy method: " <<
				to_string(section_header.copy_method) << "\n";
			ss << "        SHA256: ";
			for (size_t j = 0; j < section_header.sha256.size(); ++j)
				ss << std::hex << std::setfill('0') << std::setw(2) <<
					static_cast<unsigned>(section_header.sha256[j]);
			ss << '\n';
		}

		return ss.str();
	}

	std::optional<firm_section_encryption> guess_firm_encryption(firm& firm_)
	{
		// Check hashes to try to guess encryption
		if (firm_.sections.empty() || firm_.header.section_headers.empty())
			return {};

		auto& section = firm_.sections[0];
		auto& section_header = firm_.header.section_headers[0];

		if (section_header.sha256 == sha256sum(section))
			return {firm_section_encryption::NONE};

		std::array enum_array{firm_section_encryption::RETAIL_SPI, firm_section_encryption::DEV_SPI};
		for (auto& encryption : enum_array)
		{
			std::vector<unsigned char> section_data =
				decrypt_section(section_header, section, encryption);
			if (section_header.sha256 == sha256sum(section_data))
				return {encryption};
		}
		return {firm_section_encryption::NONE};
	}
} // namespace firmutils

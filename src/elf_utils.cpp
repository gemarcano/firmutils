// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020

#include <firmutils/elf_utils.h>

#include <libelf.h>
#include <gelf.h>

#include <memory>
#include <fstream>
#include <map>
#include <filesystem>
#include <vector>

namespace firmutils
{
	elf_wrapper::elf_wrapper(const std::vector<unsigned char>& elf_data)
	:elf_wrapper(std::vector<unsigned char>(elf_data))
	{}

	elf_wrapper::elf_wrapper(std::vector<unsigned char>&& elf_data)
	:elf_data_(std::move(elf_data))
	{
		if (elf_version(EV_NONE) == EV_NONE)
		{
			elf_version(EV_CURRENT);
		}

		elf_version(EV_CURRENT);
		elf_.reset(elf_memory(
				reinterpret_cast<char*>(elf_data_.data()), elf_data_.size()));
		Elf_Kind ek = elf_kind(elf_.get());
		if (ek != ELF_K_ELF ||
				gelf_getclass(elf_.get()) != ELFCLASS32)
		{
			elf_data_.clear();
			elf_.reset();
			return;
		}

		GElf_Ehdr ehdr;
		gelf_getehdr(elf_.get(), &ehdr);
		if (ehdr.e_machine != EM_ARM ||
				ehdr.e_type != ET_EXEC ||
				ehdr.e_phnum == 0)
		{
			elf_data_.clear();
			elf_.reset();
			return;
		}
	}

	elf_wrapper::operator bool() const
	{
		return elf_.get();
	}

	uint32_t elf_wrapper::get_entrypoint() const
	{
		GElf_Ehdr ehdr;
		gelf_getehdr(elf_.get(), &ehdr);
		return ehdr.e_entry;
	}

	uint32_t elf_wrapper::get_load_address() const
	{
		GElf_Ehdr ehdr;
		gelf_getehdr(elf_.get(), &ehdr);
		uint32_t load_address = 0xFFFFFFFFu;
		for (size_t i = 0; i < ehdr.e_phnum; ++i)
		{
			GElf_Phdr phdr;
			gelf_getphdr(elf_.get(), i, &phdr);
			if (phdr.p_paddr < load_address)
				load_address = phdr.p_paddr;
		}

		return load_address;
	}

	std::vector<unsigned char> elf_wrapper::flatten() const
	{
		std::vector<unsigned char> flat;
		std::map<uint32_t, GElf_Phdr> phdrs{};
		GElf_Ehdr ehdr{};
		gelf_getehdr(elf_.get(), &ehdr);
		for (size_t i = 0; i < ehdr.e_phnum; ++i)
		{
			GElf_Phdr phdr;
			gelf_getphdr(elf_.get(), i, &phdr);
			phdrs[phdr.p_paddr] = phdr;
		}

		// We now have enough to make a flat file
		uint32_t prev_segment_end = phdrs.begin()->second.p_paddr;
		for (auto& pair : phdrs)
		{
			auto& phdr = pair.second;
			if (phdr.p_type != PT_LOAD || phdr.p_filesz == 0)
				continue;

			if (prev_segment_end < phdr.p_paddr)
			{
				flat.resize(flat.size() + (phdr.p_paddr - prev_segment_end));
			}

			flat.insert(flat.cend(),
				&elf_data_[phdr.p_offset],
				&elf_data_[phdr.p_offset] + phdr.p_filesz);

			if (phdr.p_filesz < phdr.p_memsz)
			{
				flat.resize(flat.size() + (phdr.p_memsz - phdr.p_filesz));
			}
			prev_segment_end = phdr.p_paddr + phdr.p_memsz;
		}

		return flat;
	}
} // namespace firmutils

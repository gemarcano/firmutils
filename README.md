# firmutils

This projects is meant to provide a library and several utilities for handling
Nintendo 3DS FIRM images.

Programs
 - firmbuilder: Used to build FIRM images
 - readfirm: Used to read information from a FIRM image

Libraries
 - libfirmutils: Provides C and C++ APIs for manipulating FIRM images

## Dependencies
 - C++20 compiler
 - Meson
 - libelf (elfutils)
 - openssl
 - nohlmann\_json
 - catch2
 - cxxopts

## Building
```
mkdir build/
cd build/
meson ../
meson compile
```

## Installing

Meson is configured to install `firmbuilder` and `readfirm` as executables to
the prefix that the build directory is configured to use (see `meson configure
--help` for more information). `libfirmutils` is installed to the `lib` or
`lib64` directory (meson tries to guess, use `--libdir` with meson to specify a
specific folder if there are issues) in the aforementioned prefix. Headers are
installed to the `include/firmutils` folder of the prefix.  After meson is
configured, the following will build and install the executables,
libfirmbuilder, and the headers:

```
meson install
```

## Unit tests

After configuring the build, calling the following will build and run unit
tests:

```
meson test
```

## Repository layout

 - src:     Source code
 - include: Include directory used by source code and `libfirmutils`
 - tests:   Unit test source code
 - extra:   Extra source code, including source code for binaries in tests

## firmbuilder usage

### Examples

```
# Takes in a config foo.json to build a FIRM foo.firm
firmbuilder -o foo.firm foo.json
# Takes in a config bar.json from stdin and, alongside the -S parameter
# specifying the second section, build a FIRM bar.firm
firmbuilder -S '[{},{"copy_method": "NDMA", "binary": "tiny.elf"}]' -o bar.firm \
    < bar.json
```

### Configuration file

firmbuilder takes in a JSON configuration file, either via a file or standard
input, to build a FIRM. As an example:

```json
{
    "arm9_entry": "0x12345",
    "arm11_entry" : 123456,
    "boot_priority" : "11",
    "encryption": "NONE",
	"signature": "RETAIL_NAND",
    "sections" : [
        { "copy_method": "NDMA", "binary": "tiny.elf", "load_address": 0 },
        { "copy_method": "XDMA", "binary": "tiny2.elf"},
        { "copy_method": "CPU", "binary": "raw.bin", "load_address": 20 },
        { "copy_method": "NDMA", "binary": "raw2.bin", "load_address": 30 }
    ]
}
```

#### JSON syntax

Data types:

| Type          | Description                                                 |
|---------------|-------------------------------------------------------------|
| number        | Integers. Normally, JSON does not support hexadecimal or octal numbers. To work around this, firmbuilder understands numbers as strings as well, including hexadecimal and octal numbers as strings (i.e. "0x10" or "020" or "16" should all equal the number 16). |
| encryption    | Should be a string of either "NONE", "RETAIL\_SPI", or "DEV\_SPI". |
| copy method   | Should be a string of either "NDMA", "XDMA", or "CPU".      |
| sighax        | Should be a string of either "RETAIL\_NAND", "RETAIL\_NCSD", "RETAIL\_SPI", "DEV\_NAND", "DEV\_NCSD" or "DEV\_SPI". |
| sections      | JSON array, with a max of 4 JSON objects as elements.       |

The JSON FIRM description has the following fields:

| Entry          | Type           | Description                               |
|----------------|----------------|-------------------------------------------|
| arm9\_entry    | number         | Entrypoint for the ARM9 processor.        |
| arm11\_entry   | number         | Entrypoint for one of the ARM11 cores.    |
| boot\_priority | number         | Boot priority of the FIRM.                |
| encryption     | encryption     | Whether to encrypt the FIRM or not, and with what key |
| signature      | sighax         | Which sighax signature to use             |
| sections       | array(objects) | The sections of the FIRM.                 |

Each section (up to 4 sections) consists of the following fields:

| Entry         | Type           | Description                                |
|---------------|----------------|--------------------------------------------|
| copy\_method  | copy method    | The method to use to copy the section to memory. |
| binary        | string (path)  | The path to the file to use as the source for the program. If the path points to an ELF32 ARM program, the ELF will be parsed to extract a flattened version of the loadable sections, else the binary will be assumed to be already flattened. |
| load\_address | number         | Address to load the binary data to.        |

Some of the fields are optional in general, while others are optional depending
on the value of others:
 - arm9\_entry is optional if there is a section with an "NDMA" copy method and
   the section's binary field is a path to a valid ELF32 ARM program.
 - arm11\_entry is optional if there is a section with an "XDMA" copy method
   and the section's binary field is a path to a valid ELF32 ARM program.
 - boot\_priority is always optional. If it is not specified, firmbuilder sets
   the boot priority to 0.
 - encryption is always optional. If it is not specified, firmbuilder sets the
   encryption to "NONE".

Within each section, load\_address is optional if the binary entry points to a
valid ELF32 ARM program.

#### Command line arguments and overrides

The following command line arguments can be used to either specify missing
elements in the configuration file, or outright override them:

| Argument             | Type                         | Description           |
|----------------------|------------------------------|-----------------------|
| -N, --arm9\_entry    | number                       | Entrypoint for the ARM9 processor. |
| -E, --arm11\_entry   | number                       | Entrypoint for one ARM11 core. |
| -B, --boot\_priority | number                       | FIRM boot priority.    |
| -C, --encryption     | encryption                   | Whether to encrypt FIRM sections or not, and with which key. |
| -I, --signaturen     | sighax                       | Which sighax signature to use. If not specified, RETAIL\_NAND is used. |
| -S, --sections       | sections (string JSON array) | JSON array representing sections, see below. |

The -S/--sections argument takes in a string representing a JSON array. It has
all of the same requirements as the `"sections"` entry in the FIRM
configuration file. In addition, as command line arguments can be used to
augment or replace parts of the configuration file, firmbuilder recognizes
empty objects in the array as sections to ignore/not override. For example, the
string `'[{},{},{}, {"binary": "custom.elf"}]'` skips sections 1, 2, and 3
and either adds or overrides the `"binary"` entry in section 4.

For more general information about the CLI arguments, and what other ones are
available, refer to the help output from firmbuilder by running:
```
firmbuilder -h
```

## readfirm

`readfirm` is a utility to show information about FIRM archives. It prints out
the FIRM header and the section header information of the FIRM. It also
attempts to assess whether the FIRM is valid and whether it will boot on the
3DS if installed on the FIRM parition (this is very much a WIP, trust at your
own risk). If it finds any issues, it will print them towards the end of the
output.

`readfirm` can read in a FIRM from standard input or a file. See `firmutils -h`
for more information.

Example:
```
# From a file
readfirm test.firm
# From stdin
readfirm -s < test.firm
```

## libfirmutils

If linking with gcc or a C compiler, one must also link in the C++ standard
library (for GCC, that's stdc++).

Refer to generated Doxygen documentation for information on the API.

The C API is meant to provide an interface that is more portable than the C++
API. The C API is actually just a wrapper around the C++ API and objects, but
this detail is hidden from users, other than the requirement of linking against
a C++ standard library.

Currently, there is no API stability. Ideally, the C API will be locked down at
some point.

# Credits

 - \#GodMode9 on EFNET for keeping 3DS development going after all of these
   years, and inspiring me to do this
 - TuxSH and firmtool for providing a standard to compare against
 - \#Cakey on EFNET IRC (RIP) for all the help throughout the years

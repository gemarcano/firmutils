// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020

#include <firmutils/utils.h>
#include <nlohmann/json.hpp>

#define CATCH_CONFIG_MAIN
#include <catch2/catch_test_macros.hpp>

using namespace firmutils;

TEST_CASE("result_type assignment and initialization", "[result_type]")
{
	result_type<int, char> r;
	// The following should fail to compile
	// result_type<int, int> r2;
	// r = 2u;
	r = 2;
	REQUIRE(r);
	REQUIRE(r.get() == 2);
	r = 'a';
	REQUIRE(!r);
	REQUIRE(r.error() == 'a');
}

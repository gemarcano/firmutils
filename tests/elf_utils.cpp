// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020

#include <firmutils/elf_utils.h>

#define CATCH_CONFIG_MAIN
#include <catch2/catch_test_macros.hpp>

#include <array>
#include <fstream>
#include <vector>
#include <filesystem>
#include <iostream>

using namespace firmutils;

static uint32_t extract_le32(unsigned char *data)
{
	uint32_t result = 0;
	for (size_t i = 0; i < 4; ++i)
	{
		result |= data[i] << (8*i);
	}
	return result;
}

TEST_CASE("ELF parsing", "[elf_wrapper]")
{
	std::vector<unsigned char> elf_data;
	{
	std::filesystem::path filepath("tiny.elf");
	size_t file_size = std::filesystem::file_size(filepath);
	std::ifstream elf(filepath.string(), std::ios::binary);
	elf_data.resize(file_size);
	elf.read(reinterpret_cast<char*>(elf_data.data()), file_size);
	}

	elf_wrapper elf(elf_data);
	REQUIRE(elf);
	REQUIRE(elf.get_entrypoint() == 0x10000);
}

TEST_CASE("Flatten ELF files", "[flatten_elf]")
{
	std::vector<unsigned char> elf_data;
	{
	std::filesystem::path filepath("tiny.elf");
	size_t file_size = std::filesystem::file_size(filepath);
	std::ifstream elf(filepath.string(), std::ios::binary);
	elf_data.resize(file_size);
	elf.read(reinterpret_cast<char*>(elf_data.data()), file_size);
	}
	elf_wrapper elf(elf_data);
	REQUIRE(elf);
	auto flat = elf.flatten();
	REQUIRE(flat.size() == 12);
	// these are three opcodes:
	// mov r0, #42
	// mov r7. #1
	// swi #0
	REQUIRE(0xe3a0002a == extract_le32(flat.data()));
	REQUIRE(0xe3a07001 == extract_le32(flat.data()+4));
	REQUIRE(0xef000000 == extract_le32(flat.data()+8));

	{
	std::filesystem::path filepath("tiny2.elf");
	size_t file_size = std::filesystem::file_size(filepath);
	std::ifstream elf_file(filepath.string(), std::ios::binary);
	elf_data.resize(file_size);
	elf_file.read(reinterpret_cast<char*>(elf_data.data()), file_size);
	}
	elf = elf_wrapper(elf_data);
	REQUIRE(elf);

	flat = elf.flatten();
	REQUIRE(flat.size() == 0x10004);
	// This one is an opcode for swi #0
	REQUIRE(extract_le32(flat.data()+0x10000) == 0xef000000);
}

// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020

#include <firmutils/firmutils.h>
#include <firmutils/crypto.h>
#include <firmutils/firm.h>

#define CATCH_CONFIG_MAIN
#include <catch2/catch_test_macros.hpp>

using namespace firmutils;

static const firm_section_header section_header {
	.offset = 0x30313233,
	.load_address = 0x34353637,
	.size = 0x61626364,
	.copy_method = firm_copy_method::XDMA,
	.sha256 = {}
}; // The other parameters don't matter for encryption

// The encrypted result was calculated manually using openssl on the
// command line and a random CBC encryptor online
static const std::string foobar("foobar          ");
static const std::vector<unsigned char> foobar_data(foobar.begin(), foobar.end());
static const std::vector<unsigned char> encrypted_foobar_retail({0xe8, 0xa8,
	0x85, 0x7b, 0xbe, 0x55, 0x61, 0xd9, 0xc6, 0x92, 0x4b, 0x2e, 0x66, 0x62,
	0x50, 0x20});
static const std::vector<unsigned char> encrypted_foobar_dev({0x70, 0xa3, 0xf5,
	0x32, 0x85, 0x8a, 0x74, 0x0f, 0x7e, 0x6a, 0xca, 0xff, 0x72, 0x7a, 0xaf,
	0x17});

TEST_CASE("firmutils_encryption equivalence to firm_section_encryption", "[firmutils_encryption]")
{
	REQUIRE(static_cast<int>(FIRMUTILS_ENC_NONE) == static_cast<int>(firmutils::firm_section_encryption::NONE));
	REQUIRE(static_cast<int>(FIRMUTILS_ENC_RETAIL_SPI) == static_cast<int>(firmutils::firm_section_encryption::RETAIL_SPI));
	REQUIRE(static_cast<int>(FIRMUTILS_ENC_DEV_SPI) == static_cast<int>(firmutils::firm_section_encryption::DEV_SPI));
}

TEST_CASE("firmutils_encryption_from_string", "[from_string<firm_section_encryption>]")
{
	REQUIRE(firmutils_encryption_from_string("NONE") == FIRMUTILS_ENC_NONE);
	REQUIRE(firmutils_encryption_from_string("RETAIL_SPI") == FIRMUTILS_ENC_RETAIL_SPI);
	REQUIRE(firmutils_encryption_from_string("DEV_SPI") == FIRMUTILS_ENC_DEV_SPI);
	REQUIRE(firmutils_encryption_from_string("") == FIRMUTILS_ENC_NONE);
}

TEST_CASE("firmutils_encryption_to_string", "[firmutils_encryption_to_strings]")
{
	using namespace std::string_literals;
	REQUIRE(firmutils_encryption_to_string(FIRMUTILS_ENC_NONE) == "NONE"s);
	REQUIRE(firmutils_encryption_to_string(FIRMUTILS_ENC_RETAIL_SPI) == "RETAIL_SPI"s);
	REQUIRE(firmutils_encryption_to_string(FIRMUTILS_ENC_DEV_SPI) == "DEV_SPI"s);
}

TEST_CASE("firmutils_copy_method_from_string", "[firmutils_copy_method_from_string]")
{
	REQUIRE(firmutils_copy_method_from_string("NDMA") == FIRMUTILS_COPY_NDMA);
	REQUIRE(firmutils_copy_method_from_string("XDMA") == FIRMUTILS_COPY_XDMA);
	REQUIRE(firmutils_copy_method_from_string("CPU") == FIRMUTILS_COPY_CPU);
	REQUIRE(firmutils_copy_method_from_string("") == FIRMUTILS_COPY_NDMA);
}

TEST_CASE("firmutils_copy_method_to_string", "[firmutils_copy_method_to_string]")
{
	using namespace std::string_literals;
	REQUIRE(firmutils_copy_method_to_string(FIRMUTILS_COPY_NDMA) == "NDMA"s);
	REQUIRE(firmutils_copy_method_to_string(FIRMUTILS_COPY_XDMA) == "XDMA"s);
	REQUIRE(firmutils_copy_method_to_string(FIRMUTILS_COPY_CPU) == "CPU"s);
}

TEST_CASE("firmutils_firm_to_string", "[firmutils_firm_to_string]")
{
	/**firm_header header;
	header.section_headers.emplace_back();
	auto &section_header = header.section_headers[0];
	section_header.offset = 0x400;
	section_header.load_address = 0x12345;
	section_header.size = 9999;
	section_header.copy_method = firm_copy_method::XDMA;
	section_header.sha256[0] = 0x12;
	section_header.sha256[31] = 0x9F;

	std::string str = to_string(header);
	const auto npos = std::string::npos;
	auto pos = str.find("magic: FIRM\n");
	REQUIRE(pos != npos);
	pos = str.find("boot priority: 11\n");
	REQUIRE(pos != npos);
	pos = str.find("arm11 entrypoint: 0x123A\n");
	REQUIRE(pos != npos);
	pos = str.find("arm9 entrypoint: 0x1234B\n");
	REQUIRE(pos != npos);

	pos = str.find("    section: 0\n");
	REQUIRE(pos != npos);
	pos = str.find("        byte offset: 1024\n");
	REQUIRE(pos != npos);
	pos = str.find("        load address: 0x12345\n");
	REQUIRE(pos != npos);
	pos = str.find("        size: 9999\n");
	REQUIRE(pos != npos);
	pos = str.find("        copy method: XDMA\n");
	REQUIRE(pos != npos);
	pos = str.find("        SHA256: 120000000000000000000000000000000000000000000000000000000000009F\n");
	REQUIRE(pos != npos);*/
}

// FIXME test stream operators

TEST_CASE("Encrypt FIRM section", "[firmutils_encrypt_section]")
{
	REQUIRE(foobar.length() == 16);

	// IV should be
	// u8[] = 0x33 0x32 0x31 0x30 0x37 0x36 0x35 0x34 0x64 0x63 0x62 0x61 0x64 0x63 0x62 0x61
	// or as a string: "32107654dcbadcba"
	std::vector<unsigned char> encrypted(foobar.length());
	auto section_header_ = reinterpret_cast<const firmutils_section_header*>(&section_header);
	auto result = firmutils_encrypt_section(section_header_, foobar_data.data(), foobar_data.size(), encrypted.data(), encrypted.size(), FIRMUTILS_ENC_RETAIL_SPI);

	CHECK(result == foobar_data.size());
	CHECK(encrypted_foobar_retail == encrypted);
	result = firmutils_encrypt_section(section_header_, foobar_data.data(), foobar_data.size(), encrypted.data(), encrypted.size(), FIRMUTILS_ENC_DEV_SPI);
	CHECK(result == foobar_data.size());
	CHECK(encrypted_foobar_dev == encrypted);
}

TEST_CASE("Decrypt FIRM section", "[firmutils_decrypt_section]")
{
	REQUIRE(foobar.length() == 16);

	std::vector<unsigned char> decrypted(foobar.length());
	auto section_header_ = reinterpret_cast<const firmutils_section_header*>(&section_header);
	auto result = firmutils_decrypt_section(section_header_, encrypted_foobar_retail.data(), encrypted_foobar_retail.size(), decrypted.data(), decrypted.size(), FIRMUTILS_ENC_RETAIL_SPI);
	CHECK(foobar_data.size() == result);
	CHECK(foobar_data == decrypted);

	result = firmutils_decrypt_section(section_header_, encrypted_foobar_dev.data(), encrypted_foobar_dev.size(), decrypted.data(), decrypted.size(), FIRMUTILS_ENC_DEV_SPI);
	CHECK(foobar_data.size() == result);
	CHECK(foobar_data == decrypted);
}

TEST_CASE("Check FIRM section_header firmutils support", "[firmutils_section_header]")
{
	firmutils_section_header *header = firmutils_section_header_new();
	REQUIRE(header != nullptr);
	firmutils_section_header_set_offset(header, 1);
	firmutils_section_header_set_load_address(header, 2);
	firmutils_section_header_set_size(header, 3);
	firmutils_section_header_set_copy_method(header, FIRMUTILS_COPY_XDMA);
	std::array<unsigned char, 32> fake_sha{{0,1,2,3,4,5}};
	firmutils_section_header_set_sha256(header, fake_sha.data());

	REQUIRE(firmutils_section_header_get_offset(header) == 1);
	REQUIRE(firmutils_section_header_get_load_address(header) == 2);
	REQUIRE(firmutils_section_header_get_size(header) == 3);
	REQUIRE(firmutils_section_header_get_copy_method(header) == FIRMUTILS_COPY_XDMA);
	REQUIRE(memcmp(firmutils_section_header_get_sha256(header), fake_sha.data(), 32) == 0);

	firmutils_section_header_free(header);
	firmutils_section_header_free(nullptr);
}

TEST_CASE("Check FIRM header firmutils support", "[firmutils_header]")
{
	firmutils_header *header = firmutils_header_new();
	REQUIRE(header != nullptr);
	firmutils_header_set_magic(header, reinterpret_cast<const unsigned char*>("FAKE"));
	firmutils_header_set_boot_priority(header, 1);
	firmutils_header_set_arm11_entrypoint(header, 11);
	firmutils_header_set_arm9_entrypoint(header, 9);
	std::array<unsigned char, 256> fake_sig{{1,2,3,4,5,6,7,8,9,1}};
	firmutils_header_set_signature(header, fake_sig.data());

	REQUIRE(memcmp(firmutils_header_get_magic(header), "FAKE", 4) == 0);
	REQUIRE(firmutils_header_get_boot_priority(header) == 1);
	REQUIRE(firmutils_header_get_arm11_entrypoint(header) == 11);
	REQUIRE(firmutils_header_get_arm9_entrypoint(header) == 9);
	REQUIRE(memcmp(firmutils_header_get_signature(header), fake_sig.data(), 256) == 0);
	firmutils_header_free(header);
	firmutils_header_free(nullptr);
}

TEST_CASE("Check FIRM firmutils support", "[firmutils_firm]")
{
	firmutils_firm *firm = firmutils_firm_new();
	REQUIRE(firm != nullptr);
	firmutils_header *header = firmutils_firm_get_header(firm);
	firmutils_header *header2 = firmutils_firm_get_header(firm);
	REQUIRE(header != nullptr);
	REQUIRE(header == header2);

	firmutils_header *header3 = firmutils_header_new();
	firmutils_header_set_magic(header3, reinterpret_cast<const unsigned char*>("FAKE"));
	firmutils_header_set_boot_priority(header3, 1);
	firmutils_header_set_arm11_entrypoint(header3, 11);
	firmutils_header_set_arm9_entrypoint(header3, 9);
	std::array<unsigned char, 256> fake_sig{{1,2,3,4,5,6,7,8,9,1}};
	firmutils_header_set_signature(header3, fake_sig.data());
	firmutils_firm_set_header(firm, header3);
	firmutils_header_free(header3);

	REQUIRE(memcmp(firmutils_header_get_magic(header), "FAKE", 4) == 0);
	REQUIRE(firmutils_header_get_boot_priority(header) == 1);
	REQUIRE(firmutils_header_get_arm11_entrypoint(header) == 11);
	REQUIRE(firmutils_header_get_arm9_entrypoint(header) == 9);
	REQUIRE(memcmp(firmutils_header_get_signature(header), fake_sig.data(), 256) == 0);

	unsigned char *section_data = reinterpret_cast<unsigned char*>(1);
	size_t section_size = 1;
	firmutils_firm_get_section_data(firm, 0, &section_data, &section_size);
	REQUIRE(section_data == nullptr);
	REQUIRE(section_size == 0);

	firmutils_firm_set_section_data(firm, 1, fake_sig.data(), fake_sig.size());
	firmutils_firm_get_section_data(firm, 0, &section_data, &section_size);
	REQUIRE(section_data == nullptr);
	REQUIRE(section_size == 0);
	firmutils_firm_get_section_data(firm, 1, &section_data, &section_size);
	REQUIRE(memcmp(section_data, fake_sig.data(), 256) == 0);
	REQUIRE(section_size == 256);

	firmutils_firm_free(firm);
	firmutils_firm_free(nullptr);
}

TEST_CASE("Check FIRM parsing", "[firmutils_parse_firm]")
{
	std::vector<unsigned char> data(0x300);
	std::array<unsigned char, 256> fake_sig{{1,2,3,4,5,6,7,8,9,1}};

	memcpy(data.data(), "FAKE", 4);
	memcpy(data.data() + 4, "\x01\x00\x00\x00", 4);
	memcpy(data.data() + 8, "\x0B\x00\x00\x00", 4);
	memcpy(data.data() + 12, "\x09\x00\x00\x00", 4);
	memcpy(data.data() + 0x40, "\x00\x02\x00\x00", 4);
	memcpy(data.data() + 0x44, "\x00\x02\x00\x00", 4);
	memcpy(data.data() + 0x48, "\x00\x01\x00\x00", 4);
	memcpy(data.data() + 0x4C, "\x01\x00\x00\x00", 4);
	memcpy(data.data() + 0x50, fake_sig.data(), 32);
	memcpy(data.data() + 0x200, fake_sig.data(), 0x100);

	firmutils_firm *firm = firmutils_firm_new();
	firmutils_parse_firm(firm, data.data(), data.size());
	firmutils_header *header = firmutils_firm_get_header(firm);
	REQUIRE(memcmp(firmutils_header_get_magic(header), "FAKE", 4) == 0);
	REQUIRE(firmutils_header_get_boot_priority(header) == 1);
	REQUIRE(firmutils_header_get_arm11_entrypoint(header) == 11);
	REQUIRE(firmutils_header_get_arm9_entrypoint(header) == 9);
	firmutils_section_header *section_header = firmutils_header_get_section_header(header, 0);
	REQUIRE(firmutils_section_header_get_offset(section_header) == 0x200);
	REQUIRE(firmutils_section_header_get_load_address(section_header) == 0x200);
	REQUIRE(firmutils_section_header_get_size(section_header) == 0x100);
	REQUIRE(firmutils_section_header_get_copy_method(section_header) == FIRMUTILS_COPY_XDMA);
	REQUIRE(memcmp(firmutils_section_header_get_sha256(section_header), fake_sig.data(), 32) == 0);
	firmutils_firm_free(firm);
}

TEST_CASE("Check FIRM serialization", "[firmutils_build_firm]")
{
	firmutils_firm *firm = firmutils_firm_new();
	firmutils_header *header = firmutils_firm_get_header(firm);
	firmutils_header_set_magic(header, reinterpret_cast<const unsigned char*>("FAKE"));
	firmutils_header_set_boot_priority(header, 1);
	firmutils_header_set_arm11_entrypoint(header, 11);
	firmutils_header_set_arm9_entrypoint(header, 9);
	std::array<unsigned char, 256> fake_sig{{1,2,3,4,5,6,7,8,9,1}};
	firmutils_header_set_signature(header, fake_sig.data());
	firmutils_firm_set_section_data(firm, 0, fake_sig.data(), fake_sig.size());
	firmutils_section_header *section_header = firmutils_header_get_section_header(header, 0);
	REQUIRE(section_header != nullptr);
	firmutils_section_header_set_offset(section_header, 0x200);
	firmutils_section_header_set_load_address(section_header, 0x200);
	firmutils_section_header_set_size(section_header, 256);
	firmutils_section_header_set_copy_method(section_header, FIRMUTILS_COPY_XDMA);
	firmutils_section_header_set_sha256(section_header, fake_sig.data());

	size_t size = firmutils_build_firm(firm, nullptr, 0);
	REQUIRE(size == 0x300);
	std::vector<unsigned char> data(0x300);
	size = firmutils_build_firm(firm, data.data(), data.size());
	REQUIRE(size == 0x300);
	REQUIRE(memcmp(fake_sig.data(), data.data() + 0x200, 0x100) == 0);
	REQUIRE(memcmp("FAKE", data.data(), 4) == 0);
	REQUIRE(memcmp("\x01\x00\x00\x00", data.data() + 4, 4) == 0);
	REQUIRE(memcmp("\x0B\x00\x00\x00", data.data() + 8, 4) == 0);
	REQUIRE(memcmp("\x09\x00\x00\x00", data.data() + 12, 4) == 0);
	REQUIRE(memcmp("\x00\x02\x00\x00", data.data() + 0x040, 4) == 0);
	REQUIRE(memcmp("\x00\x02\x00\x00", data.data() + 0x044, 4) == 0);
	REQUIRE(memcmp("\x00\x01\x00\x00", data.data() + 0x048, 4) == 0);
	REQUIRE(memcmp("\x01\x00\x00\x00", data.data() + 0x04C, 4) == 0);
	REQUIRE(memcmp(fake_sig.data(), data.data() + 0x050, 32) == 0);

	firmutils_firm_free(firm);
}

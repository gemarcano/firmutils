// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020

#include <firmutils/crypto.h>
#include <firmutils/big_unsigned.h>

#define CATCH_CONFIG_MAIN
#include <catch2/catch_test_macros.hpp>

#include <array>

using namespace firmutils;

TEST_CASE("big_unsigned constructors", "[big_unsigned]")
{
	big_unsigned<1> a1(0xf0ff);
	REQUIRE(a1.get_data().size() == 1);
	REQUIRE(std::is_same_v<std::remove_cvref_t<decltype(a1.get_data())>, std::array<unsigned char, 1>>);
	REQUIRE(a1.get_data()[0] == 0xffu);
	big_unsigned<1> a2(a1);
	REQUIRE(a2.get_data()[0] == a1.get_data()[0]);
	big_unsigned<1> a3(std::move(a1));
	REQUIRE(a3.get_data()[0] == a2.get_data()[0]);
	big_unsigned<1> a4(2);
	REQUIRE(a4.get_data()[0] != a3.get_data()[0]);
	REQUIRE(a4.get_data()[0] == 2);
	a3 = big_unsigned<1>(3);
	REQUIRE(a3.get_data()[0] == 3);
	a4 = a3;
	REQUIRE(a4.get_data()[0] == 3);

	big_unsigned<4> b1(0x1000110011);
	uint32_t value = b1.get_data()[0] | b1.get_data()[1] << 8 |
		b1.get_data()[2] << 16 | b1.get_data()[3] << 24;
	REQUIRE(b1.get_data().size() == 4);
	REQUIRE(std::is_same_v<std::remove_cvref_t<decltype(b1.get_data())>, std::array<unsigned char, 4>>);
	REQUIRE(value == 0x00110011u);
	big_unsigned<4> b2(b1);
	REQUIRE(b2.get_data()[0] == b1.get_data()[0]);
	big_unsigned<4> b3(std::move(b1));
	REQUIRE(b3.get_data()[0] == b2.get_data()[0]);
	big_unsigned<4> b4(2);
	REQUIRE(b4.get_data()[0] != b3.get_data()[0]);
	REQUIRE(b4.get_data()[0] == 2);
	b3 = big_unsigned<4>(3);
	REQUIRE(b3.get_data()[0] == 3);
	b4 = b3;
	REQUIRE(b4.get_data()[0] == 3);
}

const big_unsigned<16> a1(0x1122334455667788ull);
const big_unsigned<16> a2(0xfff00f008001ff01ull);

TEST_CASE("big_unsigned operators", "[big_unsigned::operator]")
{
	big_unsigned<16> a_expected(0xEED23C44D5678889ull);
	big_unsigned<16> a3 = a1 ^ a2;
	REQUIRE(a3.get_data() == a_expected.get_data());
	a3 = big_unsigned<16>(0xfff00f008001ff01ull);
	a3 ^= a1;
	REQUIRE(a3.get_data() == a_expected.get_data());

	a_expected = big_unsigned<16>(std::array<unsigned char, 16>({{
		0x89, 0x76, 0x68, 0xD5, 0x44, 0x42, 0x12, 0x11, 0x01
	}}));
	a3 = a1 + a2;
	REQUIRE(a3.get_data() == a_expected.get_data());
	a3 = a2;
	a3 += a1;
	REQUIRE(a3.get_data() == a_expected.get_data());

	a_expected = big_unsigned<16>(0xFFF23F44D567FF89ull);
	a3 = a1 | a2;
	REQUIRE(a3.get_data() == a_expected.get_data());
	a3 = a2;
	a3 |= a1;
	REQUIRE(a3.get_data() == a_expected.get_data());

	a3 = a1 >> 32;
	REQUIRE(a3.get_data() == big_unsigned<16>(0x11223344).get_data());
	a3 = a1 >> 64;
	REQUIRE(a3.get_data() == big_unsigned<16>(0x0).get_data());
	a3 = a1 >> 0;
	REQUIRE(a3.get_data() == a1.get_data());

	a3 = a1;
	a3 >>= 12;
	REQUIRE(a3.get_data() == (a1 >> 12).get_data());

	a3 = a1 << 1;
	REQUIRE(a3.get_data() == big_unsigned<16>(0x22446688AACCEF10).get_data());
	a3 = a1 << 8;
	REQUIRE(a3.get_data()[8] == 0x11);
	REQUIRE(a3.get_data()[0] == 0x00);
	a3 = a1 << 128;
	REQUIRE(a3.get_data() == std::array<unsigned char, 16>{0});
	a3 = a1 << 124;
	REQUIRE(a3.get_data()[15] == 0x80);

	a3 = a1;
	a3 <<= 1;
	REQUIRE(a3.get_data() == big_unsigned<16>(0x22446688AACCEF10).get_data());

	a_expected = big_unsigned<16>(0xFFF23F44D567FF89);
	a3 = a1 | a2;
	REQUIRE(a3.get_data() == a_expected.get_data());
	a3 = a1;
	a3 |= a2;
	REQUIRE(a3.get_data() == a_expected.get_data());

	REQUIRE(a1 < a2);
	REQUIRE(a2 > a1);
	REQUIRE(a1 <= a2);
	REQUIRE(a2 >= a1);
	REQUIRE(a1 <= a1);
	REQUIRE(a2 >= a2);
	a3 = a2;
	REQUIRE(a2 == a3);
	REQUIRE(a1 != a2);
}

TEST_CASE("Rotate numbers", "[rotate_bits_left]")
{
	big_unsigned<16> a3 = a1;
	rotate_bits_left(a3, 64);
	REQUIRE(a3 == (a1 << 64));
	rotate_bits_left(a3, 64);
	REQUIRE(a3 == a1);

	const uint32_t b1 = 0xFF00F00Fu;
	uint32_t b2 = b1;
	rotate_bits_left(b2, 1);
	REQUIRE(b2 == 0xFE01E01Fu);
	rotate_bits_left(b2, 15);
	REQUIRE(b2 == 0xF00FFF00u);
}

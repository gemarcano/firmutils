// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020

#include <firmutils/firm.h>

#define CATCH_CONFIG_MAIN
#include <catch2/catch_test_macros.hpp>

static const firmutils::firm_section_header section_header {
	.offset = 0x30313233,
	.load_address = 0x34353637,
	.size = 0x61626364,
	.copy_method = firmutils::firm_copy_method::XDMA,
	.sha256 = {}
}; // The other parameters don't matter for encryption

// The encrypted result was calculated manually using openssl on the
// command line and a random CBC encryptor online
static const std::string foobar("foobar          ");
static const std::vector<unsigned char> foobar_data(
	foobar.begin(), foobar.end());
static const std::vector<unsigned char> encrypted_foobar_retail({0xe8, 0xa8,
	0x85, 0x7b, 0xbe, 0x55, 0x61, 0xd9, 0xc6, 0x92, 0x4b, 0x2e, 0x66, 0x62,
	0x50, 0x20});
static const std::vector<unsigned char> encrypted_foobar_dev({0x70, 0xa3, 0xf5,
	0x32, 0x85, 0x8a, 0x74, 0x0f, 0x7e, 0x6a, 0xca, 0xff, 0x72, 0x7a, 0xaf,
	0x17});

TEST_CASE(
		"from_string firm_section_encryption",
		"[from_string<firm_section_encryption>]")
{
	using namespace firmutils;
	REQUIRE(from_string<firm_section_encryption>("NONE") ==
		firm_section_encryption::NONE);
	REQUIRE(from_string<firm_section_encryption>("RETAIL_SPI") ==
		firm_section_encryption::RETAIL_SPI);
	REQUIRE(from_string<firm_section_encryption>("DEV_SPI") ==
		firm_section_encryption::DEV_SPI);
	REQUIRE_THROWS(from_string<firm_section_encryption>(""));
}

TEST_CASE(
		"to_string firm_section_encryption",
		"[to_string firm_section_encryption]")
{
	using namespace firmutils;
	REQUIRE(to_string(firm_section_encryption::NONE) == "NONE");
	REQUIRE(to_string(firm_section_encryption::RETAIL_SPI) == "RETAIL_SPI");
	REQUIRE(to_string(firm_section_encryption::DEV_SPI) == "DEV_SPI");
}

TEST_CASE("from_string firm_copy_method", "[from_string<firm_copy_method>]")
{
	using namespace firmutils;
	REQUIRE(from_string<firm_copy_method>("NDMA") == firm_copy_method::NDMA);
	REQUIRE(from_string<firm_copy_method>("XDMA") == firm_copy_method::XDMA);
	REQUIRE(from_string<firm_copy_method>("CPU") == firm_copy_method::CPU);
	REQUIRE_THROWS(from_string<firm_copy_method>(""));
}

TEST_CASE("to_string firm_copy_method", "[to_string firm_copy_method]")
{
	using namespace firmutils;
	REQUIRE(to_string(firm_copy_method::NDMA) == "NDMA");
	REQUIRE(to_string(firm_copy_method::XDMA) == "XDMA");
	REQUIRE(to_string(firm_copy_method::CPU) == "CPU");
}

TEST_CASE("to_string firm_header", "[to_string firm_header]")
{
	using namespace firmutils;
	firm_header header;
	memcpy(header.magic.data(), "FIRM", 4);
	header.boot_priority = 11;
	header.arm11_entrypoint = 0x123A;
	header.arm9_entrypoint = 0x1234B;
	header.section_headers.emplace_back();
	auto &section_header = header.section_headers[0];
	section_header.offset = 0x400;
	section_header.load_address = 0x12345;
	section_header.size = 9999;
	section_header.copy_method = firm_copy_method::XDMA;
	section_header.sha256[0] = 0x12;
	section_header.sha256[31] = 0x9F;

	std::string str = to_string(header);
	const auto npos = std::string::npos;
	auto pos = str.find("magic: FIRM\n");
	REQUIRE(pos != npos);
	pos = str.find("boot priority: 11\n");
	REQUIRE(pos != npos);
	pos = str.find("arm11 entrypoint: 0x123A\n");
	REQUIRE(pos != npos);
	pos = str.find("arm9 entrypoint: 0x1234B\n");
	REQUIRE(pos != npos);

	pos = str.find("    section 0:\n");
	REQUIRE(pos != npos);
	pos = str.find("        byte offset: 1024\n");
	REQUIRE(pos != npos);
	pos = str.find("        load address: 0x12345\n");
	REQUIRE(pos != npos);
	pos = str.find("        size: 9999\n");
	REQUIRE(pos != npos);
	pos = str.find("        copy method: XDMA\n");
	REQUIRE(pos != npos);
	pos = str.find("        SHA256: 120000000000000000000000000000000000000000000000000000000000009F\n");
	REQUIRE(pos != npos);
}

TEST_CASE(
		"Encrypt FIRM section",
		"[encrypt_section]")
{
	using namespace firmutils;
	REQUIRE(foobar.length() == 16);

	// IV should be
	// u8[] = 0x33 0x32 0x31 0x30 0x37 0x36 0x35 0x34 0x64 0x63 0x62 0x61 0x64
	// 		0x63 0x62 0x61
	// or as a string: "32107654dcbadcba"
	auto result = encrypt_section(
		section_header, foobar_data, firm_section_encryption::RETAIL_SPI);

	CHECK(encrypted_foobar_retail == result);
	result = encrypt_section(
		section_header, foobar_data, firm_section_encryption::DEV_SPI);
	CHECK(encrypted_foobar_dev == result);
}

TEST_CASE("Decrypt FIRM section", "[decrypt_section]")
{
	using namespace firmutils;
	REQUIRE(foobar.length() == 16);

	auto result = decrypt_section(
		section_header,
		encrypted_foobar_retail,
		firm_section_encryption::RETAIL_SPI);
	CHECK(foobar_data == result);

	result = decrypt_section(
		section_header,
		encrypted_foobar_dev,
		firm_section_encryption::DEV_SPI);
	CHECK(foobar_data == result);
}

TEST_CASE("Check FIRM parsing", "[firm operator>>]")
{
	// Fake FIRM data in memory
	std::vector<unsigned char> data(0x300);
	std::array<unsigned char, 256> fake_sig{{1,2,3,4,5,6,7,8,9,1}};

	// Fill in fake FIRM data
	memcpy(data.data(), "FAKE", 4);
	memcpy(data.data() + 4, "\x01\x00\x00\x00", 4);
	memcpy(data.data() + 8, "\x0B\x00\x00\x00", 4);
	memcpy(data.data() + 12, "\x09\x00\x00\x00", 4);
	memcpy(data.data() + 0x40, "\x00\x02\x00\x00", 4);
	memcpy(data.data() + 0x44, "\x00\x02\x00\x00", 4);
	memcpy(data.data() + 0x48, "\x00\x01\x00\x00", 4);
	memcpy(data.data() + 0x4C, "\x01\x00\x00\x00", 4);
	memcpy(data.data() + 0x50, fake_sig.data(), 32);
	memcpy(data.data() + 0x200, fake_sig.data(), 0x100);

	firmutils::firm firm{};
	std::stringstream ss;
	ss.write(reinterpret_cast<char*>(data.data()), 0x300);
	ss >> firm;
	firmutils::firm_header &header = firm.header;
	REQUIRE(memcmp(header.magic.data(), "FAKE", 4) == 0);
	REQUIRE(header.boot_priority == 1);
	REQUIRE(header.arm11_entrypoint == 11);
	REQUIRE(header.arm9_entrypoint == 9);
	firmutils::firm_section_header& section_header = header.section_headers[0];
	REQUIRE(section_header.offset == 0x200);
	REQUIRE(section_header.load_address == 0x200);
	REQUIRE(section_header.size == 0x100);
	REQUIRE(section_header.copy_method == firmutils::firm_copy_method::XDMA);
	REQUIRE(memcmp(section_header.sha256.data(), fake_sig.data(), 32) == 0);
}

TEST_CASE("Check FIRM serialization", "[firm operator <<]")
{
	firmutils::firm firm{};
	firmutils::firm_header& header = firm.header;
	memcpy(header.magic.data(), "FAKE", 4);
	header.boot_priority = 1;
	header.arm11_entrypoint = 11;
	header.arm9_entrypoint = 9;
	std::array<unsigned char, 256> fake_sig{{1,2,3,4,5,6,7,8,9,1}};
	header.signature = fake_sig;
	firm.sections.emplace_back(
		fake_sig.data(), fake_sig.data() + fake_sig.size());
	header.section_headers.emplace_back();
	firmutils::firm_section_header& section_header = header.section_headers[0];
	section_header.offset = 0x200;
	section_header.load_address = 0x200;
	section_header.size = 256;
	section_header.copy_method = firmutils::firm_copy_method::XDMA;
	memcpy(section_header.sha256.data(), fake_sig.data(), 32);

	std::stringstream ss;
	ss << firm;
	std::vector<unsigned char> data(0x300);
	REQUIRE(ss.str().length() == 0x300);
	ss.read(reinterpret_cast<char*>(data.data()), 0x300);
	REQUIRE(memcmp(fake_sig.data(), data.data() + 0x200, 0x100) == 0);
	REQUIRE(memcmp("FAKE", data.data(), 4) == 0);
	REQUIRE(memcmp("\x01\x00\x00\x00", data.data() + 4, 4) == 0);
	REQUIRE(memcmp("\x0B\x00\x00\x00", data.data() + 8, 4) == 0);
	REQUIRE(memcmp("\x09\x00\x00\x00", data.data() + 12, 4) == 0);
	REQUIRE(memcmp("\x00\x02\x00\x00", data.data() + 0x040, 4) == 0);
	REQUIRE(memcmp("\x00\x02\x00\x00", data.data() + 0x044, 4) == 0);
	REQUIRE(memcmp("\x00\x01\x00\x00", data.data() + 0x048, 4) == 0);
	REQUIRE(memcmp("\x01\x00\x00\x00", data.data() + 0x04C, 4) == 0);
	REQUIRE(memcmp(fake_sig.data(), data.data() + 0x050, 32) == 0);
}

TEST_CASE("Test firm check routine", "[check_firm]")
{
	// Set up a valid FIRM
	// Entrypoints set to AXIWRAM, section load address is also same address.
	std::array<unsigned char, 256> fake_sig{{1,2,3,4,5,6,7,8,9,1}};
	firmutils::firm firm{};
	firmutils::firm_header& header = firm.header;
	memcpy(header.magic.data(), "FIRM", 4);
	header.boot_priority = 1;
	header.arm11_entrypoint = 0x1FF80000u;
	header.arm9_entrypoint = 0x1FF80000u;
	header.signature = firmutils::retail_nand_firm_sig;

	firm.sections.emplace_back(
		fake_sig.data(), fake_sig.data()+fake_sig.size());

	header.section_headers.emplace_back();
	firmutils::firm_section_header& section_header = header.section_headers[0];
	section_header.offset = 0x200;
	section_header.load_address = 0x1FF80000u;
	section_header.size = 256;
	section_header.copy_method = firmutils::firm_copy_method::XDMA;
	section_header.sha256 = firmutils::sha256sum(firm.sections[0]);

	auto is_fatal = [](const std::pair<firmutils::log_level, std::string>& val)
	{
		return val.first == firmutils::log_level::FATAL;
	};

	auto is_warn = [](const std::pair<firmutils::log_level, std::string>& val)
	{
		return val.first == firmutils::log_level::WARN;
	};

	auto logs = firmutils::check_firm(
		firm, firmutils::firm_section_encryption::NONE);
	REQUIRE(!std::any_of(logs.begin(), logs.end(), is_fatal));
	REQUIRE(!std::any_of(logs.begin(), logs.end(), is_warn));

	// Now, begin breaking FIRM
	firmutils::firm bad_firm = firm;
	auto& bad_header = bad_firm.header;

	memcpy(bad_header.magic.data(), "FAKE", 4);
	logs = firmutils::check_firm(
		bad_firm, firmutils::firm_section_encryption::NONE);
	REQUIRE(std::any_of(logs.begin(), logs.end(), is_fatal));
	REQUIRE(!std::any_of(logs.begin(), logs.end(), is_warn));
	for (auto& msg : logs)
	{
		if (msg.first == firmutils::log_level::FATAL)
			REQUIRE(msg.second.find("FIRM magic check failed") !=
				std::string::npos);
	}

	bad_firm = firm;
	bad_header.arm11_entrypoint = 0;
	logs = firmutils::check_firm(
		bad_firm, firmutils::firm_section_encryption::NONE);
	REQUIRE(std::any_of(logs.begin(), logs.end(), is_warn));
	REQUIRE(!std::any_of(logs.begin(), logs.end(), is_fatal));
	for (auto& msg : logs)
	{
		if (msg.first == firmutils::log_level::WARN)
			REQUIRE(msg.second.find("3DS boot9 won't load this FIRM") !=
				std::string::npos);
	}

	bad_firm = firm;
	bad_header.arm11_entrypoint = 0x1FF80001;
	logs = firmutils::check_firm(
		bad_firm, firmutils::firm_section_encryption::NONE);
	REQUIRE(!std::any_of(logs.begin(), logs.end(), is_warn));
	REQUIRE(std::any_of(logs.begin(), logs.end(), is_fatal));
	for (auto& msg : logs)
	{
		if (msg.first == firmutils::log_level::FATAL)
			REQUIRE(msg.second.find("ARM11 entrypoint is not valid") !=
				std::string::npos);
	}

	bad_header.arm11_entrypoint = 0x0FFFFFF0;
	logs = firmutils::check_firm(
		bad_firm, firmutils::firm_section_encryption::NONE);
	REQUIRE(!std::any_of(logs.begin(), logs.end(), is_warn));
	REQUIRE(std::any_of(logs.begin(), logs.end(), is_fatal));
	for (auto& msg : logs)
	{
		if (msg.first == firmutils::log_level::FATAL)
			REQUIRE(msg.second.find("ARM11 entrypoint is not valid") !=
				std::string::npos);
	}

	bad_firm = firm;
	bad_header.arm9_entrypoint = 0x1FF80001;
	logs = firmutils::check_firm(
		bad_firm, firmutils::firm_section_encryption::NONE);
	REQUIRE(!std::any_of(logs.begin(), logs.end(), is_warn));
	REQUIRE(std::any_of(logs.begin(), logs.end(), is_fatal));
	for (auto& msg : logs)
	{
		if (msg.first == firmutils::log_level::FATAL)
			REQUIRE(msg.second.find("ARM9 entrypoint is not valid") !=
				std::string::npos);
	}

	bad_header.arm9_entrypoint = 0x1A000000;
	logs = firmutils::check_firm(
		bad_firm, firmutils::firm_section_encryption::NONE);
	REQUIRE(!std::any_of(logs.begin(), logs.end(), is_warn));
	REQUIRE(std::any_of(logs.begin(), logs.end(), is_fatal));
	for (auto& msg : logs)
	{
		if (msg.first == firmutils::log_level::FATAL)
			REQUIRE(msg.second.find("ARM9 entrypoint is not valid") !=
				std::string::npos);
	}

	bad_firm = firm;
	bad_firm.sections.clear();
	logs = firmutils::check_firm(
		bad_firm, firmutils::firm_section_encryption::NONE);
	REQUIRE(!std::any_of(logs.begin(), logs.end(), is_warn));
	REQUIRE(std::any_of(logs.begin(), logs.end(), is_fatal));
	for (auto& msg : logs)
	{
		if (msg.first == firmutils::log_level::FATAL)
			REQUIRE(msg.second.find(
					"Number of sections and section headers do not match") !=
				std::string::npos);
	}

	bad_firm = firm;
	bad_header.section_headers.clear();
	logs = firmutils::check_firm(
		bad_firm, firmutils::firm_section_encryption::NONE);
	REQUIRE(!std::any_of(logs.begin(), logs.end(), is_warn));
	REQUIRE(std::any_of(logs.begin(), logs.end(), is_fatal));
	for (auto& msg : logs)
	{
		if (msg.first == firmutils::log_level::FATAL)
			REQUIRE(msg.second.find(
					"Number of sections and section headers do not match") !=
				std::string::npos);
	}

	bad_firm = firm;
	bad_header.section_headers[0].size = 1;
	logs = firmutils::check_firm(
		bad_firm, firmutils::firm_section_encryption::NONE);
	REQUIRE(!std::any_of(logs.begin(), logs.end(), is_warn));
	REQUIRE(std::any_of(logs.begin(), logs.end(), is_fatal));
	for (auto& msg : logs)
	{
		if (msg.first == firmutils::log_level::FATAL)
			REQUIRE(msg.second.find(
					"Recorded section size does not match actual size") !=
				std::string::npos);
	}

	bad_firm = firm;
	// FCRAM is a blacklisted area
	bad_header.section_headers[0].load_address = 0x20000000;
	logs = firmutils::check_firm(
		bad_firm, firmutils::firm_section_encryption::NONE);
	REQUIRE(!std::any_of(logs.begin(), logs.end(), is_warn));
	REQUIRE(std::any_of(logs.begin(), logs.end(), is_fatal));
	for (auto& msg : logs)
	{
		if (msg.first == firmutils::log_level::FATAL)
			REQUIRE(msg.second.find(
					"section starts or ends in blacklisted zone") !=
				std::string::npos);
	}

	bad_header.section_headers[0].load_address = 0x07FFFFFC;
	bad_header.section_headers[0].size = 24;
	bad_firm.sections[0].resize(24);
	bad_header.section_headers[0].sha256 =
		firmutils::sha256sum(bad_firm.sections[0]);
	logs = firmutils::check_firm(
		bad_firm, firmutils::firm_section_encryption::NONE);
	REQUIRE(!std::any_of(logs.begin(), logs.end(), is_warn));
	REQUIRE(std::any_of(logs.begin(), logs.end(), is_fatal));
	for (auto& msg : logs)
	{
		if (msg.first == firmutils::log_level::FATAL)
			REQUIRE(msg.second.find(
					"section starts or ends in blacklisted zone") !=
				std::string::npos);
	}

	// Actually, this isn't bad, this is actually superhax, make sure we allow
	// it
	bad_header.section_headers[0].load_address = 0x07FFFFFC;
	bad_header.section_headers[0].size = 0x60;
	bad_firm.sections[0].resize(0x60);
	bad_header.section_headers[0].sha256 =
		firmutils::sha256sum(bad_firm.sections[0]);
	logs = firmutils::check_firm(
		bad_firm, firmutils::firm_section_encryption::NONE);
	REQUIRE(!std::any_of(logs.begin(), logs.end(), is_warn));
	REQUIRE(!std::any_of(logs.begin(), logs.end(), is_fatal));

	bad_firm = firm;
	bad_header.section_headers[0].load_address = 0x19000000;
	logs = firmutils::check_firm(
		bad_firm, firmutils::firm_section_encryption::NONE);
	REQUIRE(!std::any_of(logs.begin(), logs.end(), is_warn));
	REQUIRE(std::any_of(logs.begin(), logs.end(), is_fatal));
	for (auto& msg : logs)
	{
		if (msg.first == firmutils::log_level::FATAL)
			REQUIRE(msg.second.find(
					"load address is invalid or section won't fit in") !=
				std::string::npos);
	}

	// This should work, we're overlapping two regions, but they're continuous
	bad_header.section_headers[0].load_address = 0x1FF80000 - 128;
	logs = firmutils::check_firm(
		bad_firm, firmutils::firm_section_encryption::NONE);
	REQUIRE(!std::any_of(logs.begin(), logs.end(), is_warn));
	REQUIRE(!std::any_of(logs.begin(), logs.end(), is_fatal));

	// This sucks, but the only non-contiguous valid range (non-blacklisted) is
	// from VRAM to DSP RAM. This is too big for real FIRMs, but works for
	// testing this functionality.
	bad_header.section_headers[0].load_address = 0x185FFFFC;
	bad_header.section_headers[0].size = 0x1FF00004 - 0x185FFFFC;
	bad_firm.sections[0].resize(0x1FF00004 - 0x185FFFFC);
	bad_header.section_headers[0].sha256 =
		firmutils::sha256sum(bad_firm.sections[0]);
	logs = firmutils::check_firm(
		bad_firm, firmutils::firm_section_encryption::NONE);
	REQUIRE(!std::any_of(logs.begin(), logs.end(), is_warn));
	REQUIRE(std::any_of(logs.begin(), logs.end(), is_fatal));
	for (auto& msg : logs)
	{
		if (msg.first == firmutils::log_level::FATAL)
			REQUIRE(msg.second.find(
					"load address is invalid or section won't fit") !=
				std::string::npos);
	}

	bad_firm = firm;
	bad_header.section_headers[0].offset = 0x300;
	logs = firmutils::check_firm(
		bad_firm, firmutils::firm_section_encryption::NONE);
	REQUIRE(!std::any_of(logs.begin(), logs.end(), is_warn));
	REQUIRE(std::any_of(logs.begin(), logs.end(), is_fatal));
	for (auto& msg : logs)
	{
		if (msg.first == firmutils::log_level::FATAL)
			REQUIRE(msg.second.find("Bad section offset value") !=
				std::string::npos);
	}

	bad_firm = firm;
	bad_header.section_headers[0].sha256[0] = 0;
	logs = firmutils::check_firm(
		bad_firm, firmutils::firm_section_encryption::NONE);
	REQUIRE(!std::any_of(logs.begin(), logs.end(), is_warn));
	REQUIRE(std::any_of(logs.begin(), logs.end(), is_fatal));
	for (auto& msg : logs)
	{
		if (msg.first == firmutils::log_level::FATAL)
			REQUIRE(msg.second.find("sha256 does not match") !=
				std::string::npos);
	}

	bad_firm = firm;
	bad_header.signature[0] = 0;
	logs = firmutils::check_firm(
		bad_firm, firmutils::firm_section_encryption::NONE);
	REQUIRE(std::any_of(logs.begin(), logs.end(), is_warn));
	REQUIRE(!std::any_of(logs.begin(), logs.end(), is_fatal));
	for (auto& msg : logs)
	{
		if (msg.first == firmutils::log_level::WARN)
			REQUIRE(msg.second.find(
					"Using an unknown FIRM header signature") !=
				std::string::npos);
	}
}

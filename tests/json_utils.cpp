// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020

#include <firmutils/json_utils.h>
#include <nlohmann/json.hpp>

#define CATCH_CONFIG_MAIN
#include <catch2/catch_test_macros.hpp>

using namespace firmutils;

TEST_CASE("Extract values from json objects", "[extract_value]")
{
	nlohmann::json json = nlohmann::json::parse(R"({
			"a": 3,
			"b": 18446744073709551616000000000000000000000000,
			"c": "abc",
			"d": 18446744073709551615,
			"e": 9223372036854775808
	})");
	auto output = extract_value<int>(json, "a");
	REQUIRE(output);
	REQUIRE(output.get() == 3);
	output = extract_value<int>(json, "b");
	REQUIRE(!output);
	REQUIRE(output.error().what.find("is not within the type of") != std::string::npos);
	auto output64 = extract_value<uint64_t>(json, "d");
	// Can't test the following, nlohmann::json silently converts the
	// underlying number from unsigned to signed without warning`
	//output = extract_value<int>(json, "d");
	//REQUIRE(!output);
	//REQUIRE(output.error().what.find("is not within the type of") != std::string::npos);
	REQUIRE(output64);
	REQUIRE(output64.get() == 18446744073709551615ull);
	output64 = extract_value<uint64_t>(json, "e");
	REQUIRE(output64);
	REQUIRE(output64.get() == 9223372036854775808ull);
	output = extract_value<int>(json, "e");
	REQUIRE(!output);
	REQUIRE(output.error().what.find("is not within the type of") != std::string::npos);

	auto output2 = extract_value<std::string>(json, "c");
	REQUIRE(output2);
	REQUIRE(output2.get() == "abc");
	output = extract_value<int>(json, "c");
	REQUIRE(!output);
	REQUIRE(output.error().what.find("Could not convert string to a hexadecimal number") != std::string::npos);
	output2 = extract_value<std::string>(json, "a");
	REQUIRE(!output2);
	REQUIRE(output2.error().what.find("is not a string") != std::string::npos);
}

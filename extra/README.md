# How to build tiny.elf and tiny2.elf

The unit tests for `elf_util` use two ELF files to test ELF parsing and
flattening. The assembly files and linker file in this directory are the source
code for those files.

Here is how to build them (assuming arm-none-eabi-gcc is an arm compiler):
```
CC=arm-none-eabi-gcc make
```

This should create both elf files, and should match the ones being used for
unit tests.

`make clean` works to clean up the generated files.

// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020

.section .text
.align 4
.arm
.global _start

_start:
	mov r0, #42
	mov r7, #1
	b _foobar

.section .text.far
.align 4
.arm
.global _foobar
_foobar:
	swi #0

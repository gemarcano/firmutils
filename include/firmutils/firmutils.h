// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020
///@file

#ifndef FIRMUTILS_FIRMUTILS_H_
#define FIRMUTILS_FIRMUTILS_H_

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/** Represents the kind of encryption that sections can be encrypted as. */
enum firmutils_encryption
{
	FIRMUTILS_ENC_NONE,
	FIRMUTILS_ENC_RETAIL_SPI,
	FIRMUTILS_ENC_DEV_SPI
};

/** Copy method for loading FIRM to memory. */
enum firmutils_copy_method
{
	FIRMUTILS_COPY_NDMA = 0, FIRMUTILS_COPY_XDMA, FIRMUTILS_COPY_CPU
};

/** Represents a FIRM, header and data included */
typedef struct firmutils_firm firmutils_firm;

/** FIRM section header. Normally there are 4 of these in a FIRM */
typedef struct firmutils_section_header firmutils_section_header;

/** FIRM header */
typedef struct firmutils_header firmutils_header;

/** Converts string to firmutils_encryption.
 *
 * @param[in] str String to convert.
 *
 * @returns firm_section_encryption represented by the string. Returns
 *  FIRMUTILS_ENC_NONE on an unknown input string.
 */
firmutils_encryption firmutils_encryption_from_string(const char* str);

/** Converts a firmutils_encryption to a string.
 *
 * @param[in] encryption firm_section_encryption to convert.
 *
 * @returns The string representation of the firmutils_encryption. No memory
 *  management needs to happen with the returned pointer.
 */
const char* firmutils_encryption_to_string(firmutils_encryption encryption);

/** Converts string to firm_copy_method.
 *
 * Throws an std::invalid_argument exception if str can't be converted to a
 * firm_copy_method.
 *
 * @param[in] str String to convert.
 *
 * @returns firm_copy_method represented by the string. Returns
 *  FIRMUTILS_COPY_NDMA for an unknown input string.
 */
firmutils_copy_method firmutils_copy_method_from_string(const char *str);

/** Converts a firm_copy_method to a string.
 *
 * @param[in] method firm_copy_method to convert.
 *
 * @returns The string representation of the firmutils_copy_method. No memory
 *  management needs to happen with the returned pointer.
 */
const char* firmutils_copy_method_to_string(firmutils_copy_method method);


/** @name FIRM functions */
/**{@*/

/** Allocates and initializes a new FIRM object.
 *
 * This object must be freed using firmutils_firm_free().
 *
 * @returns Pointer to a newly allocated firmutils_firm object.
 */
firmutils_firm *firmutils_firm_new(void);

/** Frees a previously allocated FIRM object.
 *
 * This does nothing if firm is NULL.
 *
 * @param[in] firm FIRM pointer to deallocate.
 *
 * @post firm is deallocated and should not be used any more.
 */
void firmutils_firm_free(firmutils_firm* firm);

/** Gets a pointer to the FIRM's header.
 *
 * @param[in] firm FIRM to get the header of.
 *
 * @returns A pointer to the FIRM's header.
 */
firmutils_header* firmutils_firm_get_header(firmutils_firm *firm);

/** Sets the FIRM's header to a copy of the specified header.
 *
 * @param[out] firm Pointer of FIRM to modify the header of.
 * @param[in] header Pointer to header to copy into FIRM.
 *
 * @post The header pointed to by the argument is copied inside the given FIRM.
 */
void firmutils_firm_set_header(
	firmutils_firm *firm, const firmutils_header *header);

/** Gets the section binary data from the FIRM.
 *
 * @param[in] firm FIRM to extract section header data from.
 * @param[in] index Index of section to extract. Index must be less than 4 (or
 *  else behavior is undefined).
 * @param[out] data Pointer to a pointer to store the binary data location.
 * @param[out] size Pointer to a location to store the size of the binary data.
 *
 * @post data and size have been updated to reflect the data of the section
 *  given by the index.
 */
void firmutils_firm_get_section_data(
	firmutils_firm *firm, size_t index, unsigned char **data, size_t *size);

/** Updates the specified section binary data in the given FIRM.
 *
 * @param[out] firm FIRM to update section data in.
 * @param[in] index Index of section binary to update. Index must be less than
 *  4 (or else behavior is undefined).
 * @param[in] data Pointer to data to copy into section data.
 * @param[in] size Size of data to copy into section data.
 *
 * @post Section at the given index has been updated to the data provided.
 */
void firmutils_firm_set_section_data(
	firmutils_firm *firm, size_t index, unsigned char *data, size_t size);

/**@}*/

/** @name firmutils_section functions */
/**@{*/

/** Allocates a new firmutils FIRM header object.
 *
 * When done using the new object, it must be freed with
 * firmutils_header_free().
 *
 * @returns A new firmutils FIRM header object.
 */
firmutils_header *firmutils_header_new(void);

/** Frees the given firmutils FIRM header object.
 *
 * @param[in,out] header Header to free. If this is NULL, this operation is
 *  effectively a no-op.
 * @post Header is freed and the memory it points to should no longer be used.
 */
void firmutils_header_free(firmutils_header *header);

/** Returns the magic entry of the given FIRM header.
 *
 * @param[in] header Header to extract data from.
 *
 * @returns A pointer to the 4 byte magic entry.
 */
const unsigned char* firmutils_header_get_magic(
	const firmutils_header* header);

/** Returns the boot priority entry of the given FIRM header.
 *
 * @param[in] header Header to extract data from.
 *
 * @returns The boot priority as found in the FIRM header.
 */
uint32_t firmutils_header_get_boot_priority(const firmutils_header* header);

/** Returns the ARM11 entrypoint entry of the given FIRM header.
 *
 * @param[in] header Header to extract data from.
 *
 * @returns The ARM11 entrypoint as found in the FIRM header.
 */
uint32_t firmutils_header_get_arm11_entrypoint(const firmutils_header* header);

/** Returns the ARM9 entrypoint entry of the given FIRM header.
 *
 * @param[in] header Header to extract data from.
 *
 * @returns The ARM9 entrypoint as found in the FIRM header.
 */
uint32_t firmutils_header_get_arm9_entrypoint(const firmutils_header* header);

/** Returns the signature entry of the given FIRM header.
 *
 * @param[in] header Header to extract data from.
 *
 * @returns A pointer to the 256 byte long signature entry as found in the FIRM
 *  header.
 */
const unsigned char* firmutils_header_get_signature(
	const firmutils_header* header);

/** Sets the magic entry of the given FIRM to the data specified.
 *
 * @param[in,out] header Header to update.
 * @param[in] magic Pointer to 4 bytes of data to copy into header's magic
 *  field.
 *
 * @post 4 bytes of magic are copied into the header's magic entry.
 */
void firmutils_header_set_magic(
	firmutils_header* header, const unsigned char *magic);

/** Sets the boot priority entry of the given FIRM header to the data
 *  specified.
 *
 * @param[in,out] header Header to update.
 * @param[in] boot_priority Boot priority to write into header.
 *
 * @post boot_priority is copied into header's boot_priority entry.
 */
void firmutils_header_set_boot_priority(
	firmutils_header* header, uint32_t boot_priority);

/** Sets the ARM11 entrypoint entry of the given FIRM header to the data
 *  specified.
 *
 * @param[in,out] header Header to update.
 * @param[in] arm11_entrypoint ARM11 entrypoint to write into header.
 *
 * @post arm11_entrypoint is copied into header's arm11_entrypoint entry.
 */
void firmutils_header_set_arm11_entrypoint(
	firmutils_header* header, uint32_t arm11_entrypoint);

/** Sets the ARM9 entrypoint entry of the given FIRM header to the data
 *  specified.
 *
 * @param[in,out] header Header to update.
 * @param[in] arm9_entrypoint ARM9 entrypoint to write into header.
 *
 * @post arm9_entrypoint is copied into header's arm9_entrypoint entry.
 */
void firmutils_header_set_arm9_entrypoint(
	firmutils_header* header, uint32_t arm9_entrypoint);

/** Sets the signature entry of the given FIRM header to the data specified.
 *
 * @param[in,out] header Header to update.
 * @param[in] signature Pointer to 256 bytes of data to write into header.
 *
 * @post 256 bytes from signature are copied into header's signature entry.
 */
void firmutils_header_set_signature(
	firmutils_header* header, const unsigned char *signature);

/**@}*/

/** @name firmutils_section_header functions */
/**@{*/

/** Allocates a new firmutils_section_header object.
 *
 * When done using the new object, it must be freed with
 * firmutils_section_header_free().
 *
 * @returns A new firmutils section header object.
 */
firmutils_section_header* firmutils_section_header_new(void);

/** Frees the given firmutils_section_header object.
 *
 * @param[in,out] header Header to free. If this is NULL, this operation is
 *  effectively a no-op.
 * @post Header is freed and the memory it points to should no longer be used.
 */
void firmutils_section_header_free(firmutils_section_header *header);

/** Copies the specified section header into the given FIRM header.
 *
 * @param[in,out] header Header to write section header to.
 * @param[in] index Index of section header to write into. Must be less than 4.
 * @param[in] section_header The section header to copy.
 *
 * @post The specified section header in the FIRM header is updated with the
 * data provided by the section_header object.
 */
void firmutils_header_set_section_header(
	firmutils_header *header,
	size_t index,
	const firmutils_section_header *section_header);

/** Returns the specified section header from the given FIRM header.
 *
 * @param[in] header Header to extract section header from.
 * @param[in] index Index of section header to extract. Must be less than 4.
 *
 * @returns A pointer to the specified section header.
 */
firmutils_section_header *firmutils_header_get_section_header(
	firmutils_header *header, size_t index);

/** Gets the offset entry from the given section header.
 *
 * @param[in] header Section header to extract data from.
 *
 * @returns The offset entry from the section header.
 */
uint32_t firmutils_section_header_get_offset(
	const firmutils_section_header *header);

/** Gets the load address entry from the given section header.
 *
 * @param[in] header Section header to extract data from.
 *
 * @returns The load address entry from the section header.
 */
uint32_t firmutils_section_header_get_load_address(
	const firmutils_section_header *header);

/** Gets the size entry from the given section header.
 *
 * @param[in] header Section header to extract data from.
 *
 * @returns The size entry from the section header.
 */
uint32_t firmutils_section_header_get_size(
	const firmutils_section_header *header);

/** Gets the copy method entry from the given section header.
 *
 * @param[in] header Section header to extract data from.
 *
 * @returns The copy method entry from the section header.
 */
firmutils_copy_method firmutils_section_header_get_copy_method(
	const firmutils_section_header *header);

/** Gets the sha256 entry from the given section header.
 *
 * @param[in] header Section header to extract data from.
 *
 * @returns A pointer to 32 bytes of the sha256 entry from the section header.
 */
const unsigned char* firmutils_section_header_get_sha256(
	const firmutils_section_header *header);

/** Sets the offset entry of the given section header to the value specified.
 *
 * @param[in,out] header section header to update.
 * @param[in] offset Value to write into section header.
 *
 * @post The section header's offset field is updated with the value specified.
 */
void firmutils_section_header_set_offset(
	firmutils_section_header *header, uint32_t offset);

/** Sets the load address entry of the given section header to the value
 *  specified.
 *
 * @param[in,out] header section header to update.
 * @param[in] load_address Value to write into section header.
 *
 * @post The section header's load address field is updated with the value
 *  specified.
 */
void firmutils_section_header_set_load_address(
	firmutils_section_header *header, uint32_t load_address);

/** Sets the size entry of the given section header to the value specified.
 *
 * @param[in,out] header section header to update.
 * @param[in] size Value to write into section header.
 *
 * @post The section header's size field is updated with the value specified.
 */
void firmutils_section_header_set_size(
	firmutils_section_header *header, uint32_t size);

/** Sets the copy method entry of the given section header to the value
 *  specified.
 *
 * @param[in,out] header section header to update.
 * @param[in] method Value to write into section header.
 *
 * @post The section header's copy_method field is updated with the value
 *  specified.
 */
void firmutils_section_header_set_copy_method(
	firmutils_section_header *header, firmutils_copy_method method);

/** Sets the sha256 entry of the given section header to the value specified.
 *
 * @param[in,out] header section header to update.
 * @param[in] sha256 Pointer to 32 bytes of data to write into section header.
 *
 * @post The section header's sha256 field is updated with 32 bytes from the
 *  given buffer.
 */
void firmutils_section_header_set_sha256(
	firmutils_section_header *header, const unsigned char* sha256);

/**@}*/

/** Copies a human-readable string representation of the FIRM header into the
 *  specified buffer.
 *
 * @param[in] header FIRM header to convert to a string.
 * @param[out] buf Buffer to copy representation to. Can be null, at which
 *  point the size of a buffer to be able to hold the result.
 * @param[in] buf_size The size of the given buffer.
 *
 * @returns The number of bytes copied. If buf is NULL, this is the number of
 *  bytes the buffer needs to be able to hold the full string conversion.
 */
size_t firmutils_firm_to_string(
	const firmutils_header *header, unsigned char *buf, size_t buf_size);

/** Copies the binary representation of the given FIRM object into the given
 *  buffer.
 *
 * @param[in] firm FIRM to use to build binary image.
 * @param[out] buf Buffer to store binary image in. If this is NULL, the
 * function returns the number of bytes a buffer would have to be to hold the
 *  binary image.
 * @param[in] buf_size Size of bufferbuf Buffer to store binary image in.
 *
 * @returns The number of bytes copied. If buf is NULL, this is the number of
 *  bytes the buffer needs to be to be able to hold the binary image.
 */
size_t firmutils_build_firm(
	const firmutils_firm *firm, unsigned char *buf, size_t buf_size);

/** Converts the binary representation of a FIRM into a FIRM object.
 *
 * @param[out] firm FIRM object to copy the result to.
 * @param[in] buf Buffer with binary FIRM image.
 * @param[in] buf_size Size of buffer.
 *
 * @returns A reference to the os stream object.
 */
size_t firmutils_parse_firm(
	firmutils_firm *firm, const unsigned char *buf, size_t buf_size);

/** Encrypts a FIRM section.
 *
 * @param[in] header Header of section to encrypt.
 * @param[in] src Raw data of section to encrypt.
 * @param[in] src_size Size of the raw buffer to encrypt.
 * @param[out] dst Destination buffer to save result to.
 * @param[out] dst_size Size of the destination buffer.
 * @param[in] encryption Type of FIRM encryption to encrypt as.
 *
 * @returns The number of bytes copied into the destination buffer.
 */
size_t firmutils_encrypt_section(
	const firmutils_section_header *header,
	const unsigned char* src,
	size_t src_size,
	unsigned char *dst,
	size_t dst_size,
	firmutils_encryption encryption);

/** Decrypts a FIRM section.
 *
 * @param[in] header Header of section to decrypt.
 * @param[in] src Encrypted data of section to decrypt.
 * @param[in] src_size Size of the encrypted buffer to decrypt.
 * @param[out] dst Destination buffer to save result to.
 * @param[out] dst_size Size of the destination buffer.
 * @param[in] encryption Type of FIRM encryption to decrypt as.
 *
 * @returns The number of bytes copied into the destination buffer.
 */
size_t firmutils_decrypt_section(
	const firmutils_section_header *header,
	const unsigned char* src,
	size_t src_size,
	unsigned char *dst,
	size_t dst_size,
	firmutils_encryption encryption);

/** Checks whether the given FIRM is valid.
 *
 * Checks that:
 *  - magic field is "FIRM"
 *  - the number of section headers matches the number of sections
 *  - each section doesn't have a start or end area in a boot9 blacklisted
 *    area
 *  - each section's area field in the header matches the actual size
 *  - each section's offset makes sense
 *  - each section's hash is valid
 *
 * @param[in] firm FIRM to check.
 * @param[in] encryption Encryption of FIRM.
 *
 * @returns True if it is good, false otherwise.
 */
bool firmutils_check_firm(
	const firmutils_firm *firm, firmutils_encryption encryption);

#ifdef __cplusplus
}
#endif

#endif//FIRMUTILS_FIRMUTILS_H_

// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020
///@file

#ifndef FIRMUTILS_ELF_UTILS_HPP_
#define FIRMUTILS_ELF_UTILS_HPP_

#include <libelf.h>
#include <memory>
#include <vector>

namespace firmutils
{
	/** Helper Elf deleter, for unique_ptr. */
	class elf_deleter
	{
	public:
		/** Destructor callable function.
		 *
		 * @param[in] elf Elf pointer to free.
		 *
		 * @post Given pointer is invalid since it was freed.
		 */
		void operator()(Elf *elf)
		{
			elf_end(elf);
		}
	};

	/** Helper typedef to a unique pointer with custom Elf deleter */
	using elf_unique_ptr = std::unique_ptr<Elf, elf_deleter>;

	/** Wrapper around ELF data */
	class elf_wrapper
	{
	public:
		/** Default constructor, empty wrapper. */
		elf_wrapper() = default;

		/** Constructor.
		 *
		 * @param[in] elf_data Raw binary data from an ELF file.
		 *
		 * @post If the data is a valid ELF archive, the instance's
		 *  operator bool() will return true, false otherwise.
		 */
		explicit elf_wrapper(const std::vector<unsigned char>& elf_data);

		/** Constructor.
		 *
		 * @param[in] elf_data Raw binary data from an ELF file.
		 *
		 * @post If the data is a valid ELF archive, the instance's
		 *  operator bool() will return true, false otherwise.
		 */
		explicit elf_wrapper(std::vector<unsigned char>&& elf_data);

		/** Returns whether this wrapper contains a valid ELF archive.
		 *
		 * All of the other functions rely on this being true. If there is not
		 * a valid underlying ELF, all other functions are effectively
		 * undefined.
		 *
		 * @returns True if the data contained by this wrapper is a valid ELF
		 * archive, false otherwise.
		 */
		operator bool() const;

		/** Returns the entrypoint of the ELF.
		 *
		 * @returns The entrypoint of the ELF.
		 */
		uint32_t get_entrypoint() const;

		/** Returns the load address for the section lowest in memory.
		 *
		 * @returns The load address for the section lowest in memory. This is
		 *  also equivalent to the load address of the flattened binary
		 *  returned by flatten().
		 */
		uint32_t get_load_address() const;

		/** Returns a flattened version of the ELF.
		 *
		 * Loadable segments are flattened. Anything that's not loadable is
		 * ignored (e.g. BSS).
		 *
		 * @returns A flat binary of the ELF.
		 */
		std::vector<unsigned char> flatten() const;

	private:
		std::vector<unsigned char> elf_data_;
		elf_unique_ptr elf_;
	};
} // namespace firmutils

#endif//FIRMUTILS_ELF_UTILS_HPP_

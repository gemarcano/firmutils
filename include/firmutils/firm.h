// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020
///@file

#ifndef FIRMUTILS_FIRM_H_
#define FIRMUTILS_FIRM_H_

#include <firmutils/crypto.h>
#include <firmutils/utils.h>
#include <firmutils/log.h>

#include <string_view>
#include <array>
#include <vector>
#include <istream>
#include <ostream>
#include <optional>

namespace firmutils
{
	/** Represents the kind of encryption that sections can be encrypted as. */
	enum class firm_section_encryption
	{
		NONE, RETAIL_SPI, DEV_SPI
	};

	/** Array of pointers to 3DS sighax signatures */
	extern const std::array<const std::array<unsigned char, 256>*, 6>
		sighax_signatures;

	/** Enumerations representing the 6 kinds of sighax signatures */
	enum class sighax_signature
	{
		RETAIL_NAND, RETAIL_NCSD, RETAIL_SPI,
		DEV_NAND, DEV_NCSD, DEV_SPI
	};

	/** Converts a sighax enumeration to a string.
	 *
	 * @param[in] signature Signature identifier to convert.
	 *
	 * @returns A string representing the given signature.
	 */
	constexpr std::string_view to_string(sighax_signature signature)
	{
		switch (signature)
		{
		case sighax_signature::RETAIL_NCSD:
			return "RETAIL_NCSD";
		case sighax_signature::RETAIL_SPI:
			return "RETAIL_SPI";
		case sighax_signature::DEV_NAND:
			return "DEV_NAND";
		case sighax_signature::DEV_NCSD:
			return "DEV_NCSD";
		case sighax_signature::DEV_SPI:
			return "DEV_SPI";
		case sighax_signature::RETAIL_NAND:
		default:
			return "RETAIL_NAND";
		}
	}

	/** Converts a string to a sighax_signature.
	 *
	 * Throws an std::invalid_argument if the string can't be parsed as a
	 * sighax_signature.
	 *
	 * @param[in] str String to convert.
	 *
	 * @returns The sighax_signature associated with the given string.
	 */
	template<>
	constexpr
	sighax_signature from_string<sighax_signature>(std::string_view str)
	{
		if (str == "RETAIL_NAND")
			return sighax_signature::RETAIL_NAND;
		if (str == "RETAIL_NCSD")
			return sighax_signature::RETAIL_NCSD;
		if (str == "RETAIL_SPI")
			return sighax_signature::RETAIL_SPI;
		if (str == "DEV_NAND")
			return sighax_signature::DEV_NAND;
		if (str == "DEV_NCSD")
			return sighax_signature::DEV_NCSD;
		if (str == "DEV_SPI")
			return sighax_signature::DEV_SPI;
		throw std::invalid_argument(std::string(str));
	}

	/** Converts string to firm_section_encryption.
	 *
	 * Throws an std::invalid_argument if the string can't be parsed as a
	 * firm_section_encryption.
	 *
	 * @param[in] str String to convert.
	 *
	 * @returns firm_section_encryption represented by the string.
	 */
	template<>
	constexpr firm_section_encryption
	from_string<firm_section_encryption>(std::string_view str)
	{
		if (str == "NONE")
			return firm_section_encryption::NONE;
		if (str == "RETAIL_SPI")
			return firm_section_encryption::RETAIL_SPI;
		if (str == "DEV_SPI")
			return firm_section_encryption::DEV_SPI;
		throw std::invalid_argument(std::string(str));
	}

	/** Converts a firm_section_encryption to a string.
	 *
	 * @param[in] encryption firm_section_encryption to convert.
	 *
	 * @returns The string representation of the firm_section_encryption.
	 */
	constexpr std::string_view to_string(firm_section_encryption encryption)
	{
		switch (encryption)
		{
		case firm_section_encryption::RETAIL_SPI:
			return "RETAIL_SPI";
		case firm_section_encryption::DEV_SPI:
			return "DEV_SPI";
		case firm_section_encryption::NONE:
		default:
			return "NONE";
		}
	}

	/** Copy method for loading FIRM to memory. */
	enum class firm_copy_method
	{
		NDMA = 0, XDMA, CPU
	};

	/** Converts string to firm_copy_method.
	 *
	 * Throws an std::invalid_argument exception if str can't be converted to a
	 * firm_copy_method.
	 *
	 * @param[in] str String to convert.
	 *
	 * @returns firm_copy_method represented by the string.
	 */
	template<>
	constexpr
	firm_copy_method from_string<firm_copy_method>(std::string_view str)
	{
		if (str == "NDMA")
			return firm_copy_method::NDMA;
		if (str == "XDMA")
			return firm_copy_method::XDMA;
		if (str == "CPU")
			return firm_copy_method::CPU;
		throw std::invalid_argument(std::string(str));
	}

	/** Converts a firm_copy_method to a string.
	 *
	 * @param[in] method firm_copy_method to convert.
	 *
	 * @returns The string representation of the firm_copy_method.
	 */
	constexpr std::string_view to_string(firm_copy_method method)
	{
		switch (method)
		{
		case firm_copy_method::NDMA:
			return "NDMA";
		case firm_copy_method::XDMA:
			return "XDMA";
		case firm_copy_method::CPU:
		default:
			return "CPU";
		}
	}

	/** FIRM section header. Normally there are 4 of these in a FIRM */
	struct firm_section_header
	{
		/** Offset of actual section data in FIRM file from the beginning of
		 *  thefile
		 */
		uint32_t offset;
		/** Address to load this section to, depends on copy method */
		uint32_t load_address;
		/** Size of section */
		uint32_t size;
		/** Copy method for section */
		firm_copy_method copy_method;
		/** SHA256 checksum of section data. Section data can be encrypted, and
		 * this checksum is of the plaintext */
		std::array<unsigned char, 0x20> sha256;
	};

	/** FIRM header */
	struct firm_header
	{
		/** Magic field of FIRM, should be "FIRM" */
		std::array<unsigned char, 4> magic;
		/** Boot priority field, default is 0 */
		uint32_t boot_priority;
		/** ARM11 entrypoint for one of the cores */
		uint32_t arm11_entrypoint;
		/** ARM9 entrypoint */
		uint32_t arm9_entrypoint;
		// reserved, 0x030 bytes
		/** Section headers, there should be a max of 4 */
		std::vector<firm_section_header> section_headers;
		/** FIRM signature. Use one of the sighax sigs to make sure boot9 loads
		 * FIRM properly.
		 */
		std::array<unsigned char, 0x100> signature;
	};

	/** Returns a human-readable string representation of the FIRM header.
	 *
	 * @param[in] header FIRM header to convert to a string.
	 *
	 * @returns A human-readable string representation of the FIRM header.
	 */
	std::string to_string(const firm_header &header);

	/** Represents a FIRM, header and data included */
	struct firm
	{
		/** FIRM header data */
		firm_header header;
		/** Actual binary section data */
		std::vector<std::vector<unsigned char>> sections;
	};

	/** Formatted stream operator for extracting a FIRM from the given stream.
	 *
	 * This function does not seek to the beginning of the stream to extract
	 * FIRM-- it assumes the current position points to the beginning of the
	 * FIRM header (or rather, it assumes that the current position in the
	 * stream is analogous to the 0x0 offset in the FIRM).
	 *
	 * @param[in,out] is Stream to extract FIRM from.
	 * @param[out] firm FIRM object to store result into.
	 *
	 * @returns A reference to the is stream object.
	 */
	std::istream& operator>>(std::istream& is, firm& firm);

	/** Formatted stream operator for writing a FIRM to the given stream.
	 *
	 * @param[in,out] os Stream to write FIRM to.
	 * @param[in] firm FIRM object to write to the stream.
	 *
	 * @returns A reference to the os stream object.
	 */
	std::ostream& operator<<(std::ostream& os, const firm& firm);

	/** Encrypts a FIRM section.
	 *
	 * @param[in] header Header of section to encrypt.
	 * @param[in] data Raw data of section to encrypt.
	 * @param[in] key_type Type of FIRM encryption to encrypt as.
	 *
	 * @returns Encrypted section.
	 */
	std::vector<unsigned char> encrypt_section(
		const firm_section_header& header,
		const std::vector<unsigned char>& data,
		firm_section_encryption key_type);

	/** Decrypts a FIRM section.
	 *
	 * @param[in] header Header of section to decrypt.
	 * @param[in] data Raw data of section to decrypt.
	 * @param[in] key_type Type of FIRM encryption to decrypt as.
	 *
	 * @returns Decrypted section.
	 */
	std::vector<unsigned char> decrypt_section(
		const firm_section_header& header,
		const std::vector<unsigned char>& data,
		firm_section_encryption key_type);

	/** Checks whether the given FIRM is valid.
	 *
	 * Checks that:
	 *  - magic field is "FIRM"
	 *  - the number of section headers matches the number of sections
	 *  - each section doesn't have a start or end area in a boot9 blacklisted
	 *    area
	 *  - each section's area field in the header matches the actual size
	 *  - each section's offset makes sense
	 *  - each section's hash is valid
	 *
	 * @param[in] firm_ FIRM to check.
	 * @param[in] encryption Encryption of FIRM.
	 *
	 * @returns A list of log messages. Failure is indicated by the precense of
	 *  log_level::FATAL messages.
	 */
	log_messages check_firm(
		const firm& firm_, firm_section_encryption encryption);

	std::optional<firm_section_encryption> guess_firm_encryption(firm& firm_);
} // namespace firmutils

#endif//FIRMUTILS_FIRM_H_

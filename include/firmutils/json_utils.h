// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020
///@file

#ifndef FIRMUTILS_JSON_UTILS_H_
#define FIRMUTILS_JSON_UTILS_H_

#include <firmutils/utils.h>
#include <firmutils/log.h>

#include <nlohmann/json.hpp>

#include <vector>
#include <cstdint>
#include <sstream>
#include <type_traits>
#include <string>

namespace firmutils
{
	/** Represents an error from a JSON routine */
	struct json_error
	{
		/** String describing the error encountered */
		std::string what;
	};

	/** Extracts a value from a JSON object.
	 *
	 * This function has to be specialized for every type of interest.
	 *
	 * @tparam T Type of the value to extract.
	 *
	 * @param[in] config JSON object to parse.
	 * @param[in] key key of the JSON element to extract.
	 *
	 * @returns The value extracted.
	 */
	template<class T>
	result_type<std::enable_if_t<!std::is_integral_v<T>, T>, json_error>
		extract_value(const nlohmann::json& config, const std::string& key)
	{
		return T::unimplemented;
	}

	///@cond DETAIL
	namespace detail
	{
		template<class T>
		std::enable_if_t<std::is_unsigned_v<T>,T> ston(
				const std::string& str, std::size_t* pos = 0, int base = 10)
		{
			unsigned long long converted;
			converted = std::stoull(str, pos, base);
			if (converted > std::numeric_limits<T>::max())
				throw std::out_of_range("Value is too high");
			if (converted < std::numeric_limits<T>::lowest())
				throw std::out_of_range("Value is too low");
			return static_cast<T>(converted);
		}

		template<class T>
		std::enable_if_t<std::is_signed_v<T>,T> ston(
				const std::string& str, std::size_t* pos = 0, int base = 10)
		{
			long long converted;
			converted = std::stoll(str, pos, base);
			if (converted > std::numeric_limits<T>::max())
				throw std::out_of_range("Value is too high");
			if (converted < std::numeric_limits<T>::lowest())
				throw std::out_of_range("Value is too low");
			return static_cast<T>(converted);
		}
	}
	///@endcond

	/** Extracts an integral value from a JSON object.
	 *
	 * The integral value can be a literal integral, or an integral in the form
	 * of a string (string literals can also be in hexadecimal or octal
	 * formats).
	 *
	 * @tparam T Integral type to extract.
	 *
	 * @param[in] config JSON object to parse.
	 * @param[in] key key of the JSON element to extract.
	 *
	 * @returns The value extracted, or an error if encountered.
	 */
	template<class T>
	result_type<std::enable_if_t<std::is_integral_v<T>, T>, json_error>
		extract_value(const nlohmann::json& config, const std::string& key)
	{
		result_type<T, json_error> result;
		if (config.contains(key))
		{
			auto& value = config[key];
			if (!value.is_number() && !value.is_string())
			{
				std::stringstream ss;
				ss << "\"" << key << "\": \"" << value <<
					"\" is not an integer or hex string";
				result = json_error{ss.str()};
				return result;
			}
			if (value.is_string())
			{
				std::string number = value.get<std::string>();
				size_t size;
				T converted;
				try
				{
					converted = detail::ston<T>(number, &size, 0);
				}
				catch (std::invalid_argument& e)
				{
					std::stringstream ss;
					ss <<
						"Could not convert string to a hexadecimal number: " <<
						e.what();
					result = json_error{ss.str()};
					return result;
				}
				catch (std::out_of_range& e)
				{
					std::stringstream ss;
					ss << "Specified hex number is too large to convert: " <<
						e.what();
					result = json_error{ss.str()};
					return result;
				}
				if (size != number.length())
				{
					std::stringstream ss;
					ss << "Could not process full length of hex string \"" <<
						number << "\"";
					result = json_error{ss.str()};
					return result;
				}
				result = converted;
			}
			else if (value.is_number())
			{
				using max_type =
					std::conditional_t<
						std::is_signed_v<T>, intmax_t, uintmax_t>;
				max_type number = value.get<max_type>();
				if (number < std::numeric_limits<T>::lowest() ||
						number > std::numeric_limits<T>::max())
				{
					std::stringstream ss;
					ss << "\""<< key <<"\": \"" << number <<
						"\" is not within the type of integer expected";
					result = json_error{ss.str()};
					return result;
				}
				result = static_cast<T>(number);
			}
			return result;
		}

		std::stringstream ss;
		ss << "\"" << key << "\" entry is missing";
		result = json_error{ss.str()};
		return result;
	}

	/** Extracts a string value from a JSON object.
	 *
	 * @param[in] config JSON object to parse.
	 * @param[in] key key of the JSON element to extract.
	 *
	 * @returns The value extracted, or an error if encountered.
	 */
	template<>
	result_type<std::string, json_error> extract_value<std::string>(
		const nlohmann::json& config, const std::string& key)
	{
		result_type<std::string, json_error> result;
		if (config.contains(key))
		{
			auto& value = config[key];
			if (!value.is_string())
			{
				std::stringstream ss;
				ss << "\"" << key << "\" is not a string";
				result = json_error{ss.str()};
				return result;
			}
			result = value.get<std::string>();
			return result;
		}

		std::stringstream ss;
		ss << "\"" << key << "\" entry is missing";
		result = json_error{ss.str()};
		return result;
	}

	/** Extracts a JSON element from a JSON object.
	 *
	 * @param[in] config JSON object to parse.
	 * @param[in] key key of the JSON element to extract.
	 *
	 * @returns The value extracted, or an error if encountered.
	 */
	template<>
	result_type<nlohmann::json, json_error> extract_value<nlohmann::json>(
		const nlohmann::json& config, const std::string& key)
	{
		result_type<nlohmann::json, json_error> result;
		if (config.contains(key))
		{
			auto& value = config[key];
			if (value.is_null())
			{
				std::stringstream ss;
				ss << "\"" << key << "\" is null";
				result = json_error{ss.str()};
				return result;
			}
			result = value;
			return result;
		}

		std::stringstream ss;
		ss << "\"" << key << "\" entry is missing";
		result = json_error{ss.str()};
		return result;
	}

	/** Merge two JSON objects.
	 *
	 * The first of the parameters is the "base". All additions/changes from
	 * the second are added/modified on the base. Both objects and arrays are
	 * iterated through.
	 *
	 * @param[in] base Base JSON to which the source JSON is applied.
	 * @param[in] source Source JSON to merge into base.
	 *
	 * @returns Merged JSON.
	 */
	inline nlohmann::json merge_json(
			nlohmann::json base, const nlohmann::json& source)
	{
		if (base.is_null())
			return source;
		if (source.is_null())
			return base;
		if (base.type() != source.type())
		{
			base = source;
			return base;
		}

		if (base.is_object())
		{
			for (auto& [key, value] : source.items())
			{
				if (base.count(key))
					base[key] = merge_json(base[key], value);
				else
					base[key] = value;
			}
		}
		else if (base.is_array())
		{
			for (size_t i = 0; i < source.size(); ++i)
			{
				if (i < base.size())
					base[i] = merge_json(base[i], source[i]);
				else
					base.emplace_back(source[i]);
			}
		}
		else
			base = source;

		return base;
	}

	/** Handles reading a JSON value from a JSON object.
	 *
	 * @param[out] value Place to store the requested JSON value, if found and
	 *  valid.
	 * @param[out] messages Place to append any messages added by this
	 *  function.
	 * @param[in] json JSON object to access.
	 * @param[in] key Key of value to read from JSON object.
	 * @param[in] optional If true, makes sure that if the key isn't found that
	 *  doesn't raise an error.
	 *
	 * @returns True if the value was updated, false otherwise.
	 *
	 * @post If any errors were found, messages were appended to messages with
	 *  the reason.
	 */
	template<class T>
	bool handle_json_value(
			T& value,
			log_messages& messages,
			const nlohmann::json& json,
			const std::string& key,
			bool optional = false)
	{
		auto result_ =
			extract_value<std::remove_cvref_t<T>>(json, key);
		if (!result_)
		{
			const auto& error_str = result_.error().what;
			// Only emit errors if value isn't optional or the error is not
			// that the value is missing.
			if (!optional ||
					(error_str.find(" entry is missing") == std::string::npos))
				messages.emplace_back(log_level::FATAL, error_str);
			return false;
		}
		value = result_.get();
		return true;
	}
} // namespace firmutils

#endif//FIRMUTILS_JSON_UTILS_H_

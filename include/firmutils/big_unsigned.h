// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020
///@file

#ifndef FIRMUTILS_BIG_UNSIGNED_H_
#define FIRMUTILS_BIG_UNSIGNED_H_

#include <iostream>
#include <array>
#include <iomanip>
#include <cstdint>

namespace firmutils
{
	/** Rotate bits left. Any bits carried off the left of the number are added
	 * back on the right.
	 *
	 * @param[in,out] number Value to rotate. Updated in place.
	 * @param[in] rotate Number of bits to rotate by.
	 */
	template<class Num>
	constexpr void rotate_bits_left(Num& number, size_t rotate)
	{
		Num overflow = number >> (sizeof(number) * 8 - rotate);
		number <<= rotate;
		number |= overflow;
	}

	/** Helper function to print unsigned char arrays as hex values, starting
	 *  with highest byte in array first (display numbers in human readable
	 *  form, so high bytes first).
	 *
	 * @param[in] number Number to display (as stored in an array).
	 */
	template<size_t Size>
	void print_number(const std::array<unsigned char, Size>& number)
	{
		for (size_t i = 0; i < Size; ++i)
		{
			std::cout << std::hex << std::setfill('0') << std::setw(2) <<
				(int)(number[Size-1-i]);
		}
		std::cout << std::endl;
	}

	/** Represents an unsigned number with the given number of bytes for its
	 * precision.
	 *
	 * For example, a big_unsigned with 10 bytes has 8*10 bits of precision.
	 *
	 * @tparam bytes The number of bytes to use for the number type.
	 */
	template<size_t bytes>
	class big_unsigned
	{
	public:
		/** Type of storage */
		using value_type = std::array<unsigned char, bytes>;

		/** Default constructor. Data is not initialized. */
		constexpr big_unsigned() = default;

		/** Constructor.
		 *
		 * @param[in] data Array to copy as data for the number. The lowest
		 *  byte is the least significant.
		 */
		constexpr explicit big_unsigned(const value_type& data)
		:data_(data)
		{}

		/** Default copy constructor. */
		constexpr big_unsigned(const big_unsigned&) = default;

		/** Default move constructor. */
		constexpr big_unsigned(big_unsigned&&) = default;

		/** Constructor. Initialize by loading an existing unsigned integer.
		 *
		 * @param[in] number Unsigned value to initialize this to.
		 */
		constexpr explicit big_unsigned(uintmax_t number)
		{
			for (size_t i = 0; i < std::min(sizeof(number), bytes); ++i)
			{
				data_[i] = number >> (8*i);
			}
			for (size_t i = sizeof(number); i < bytes; ++i)
			{
				data_[i] = 0;
			}
		}

		/** Default copy assignment operator.
		 *
		 * @return Reference to object assigned.
		 */
		constexpr big_unsigned& operator=(const big_unsigned&) = default;

		/** Default move assignment operator.
		 *
		 * @return Reference to object assigned.
		 */
		constexpr big_unsigned& operator=(big_unsigned&&) = default;

		/** XOR operator.
		  *
		 * @param[in] number Value to XOR against.
		 *
		 * @returns XOR of this and number.
		 */
		constexpr big_unsigned operator^(const big_unsigned& number) const
		{
			big_unsigned result(*this);
			result ^= number;
			return result;
		}

		/** XOR assignment operator.
		 *
		 * @param[in] number Value to XOR against this object..
		 *
		 * @returns A reference to this object.
		 */
		constexpr big_unsigned& operator^=(const big_unsigned& number)
		{
			for (size_t i = 0; i < bytes; ++i)
			{
				data_[i] ^= number.data_[i];
			}
			return *this;
		}

		/** Addition assignment operator.
		 *
		 * @param[in] number Value to add to this object.
		 *
		 * @returns A reference to this object.
		 */
		constexpr big_unsigned& operator+=(const big_unsigned& number)
		{
			unsigned carry = 0;
			for (size_t i = 0; i < bytes; ++i)
			{
				unsigned sum = data_[i] + number.data_[i] + carry;
				data_[i] = sum;
				carry = sum > 255;
			}
			return *this;
		}

		/** Addition operator.
		 *
		 * @param[in] number Number to add this object with.
		 *
		 * @returns The sum of this object and number.
		 */
		constexpr big_unsigned operator+(const big_unsigned& number) const
		{
			big_unsigned result(*this);
			result += number;
			return result;
		}

		/** Left shift assignment operator.
		 *
		 * @param[in] n Number of bits to shift left.
		 *
		 * @returns A reference to this object.
		 */
		constexpr big_unsigned& operator<<=(size_t n)
		{
			for (size_t i = 0; i < n; ++i)
			{
				int prev_carry = 0;
				for (size_t byte = 0; byte < bytes; ++byte)
				{
					int carry = !!(data_[byte] & 0x80);
					data_[byte] <<= 1;
					data_[byte] |= prev_carry;
					prev_carry = carry;
				}
			}
			return *this;
		}

		/** Left shift operator.
		 *
		 * @param[in] n Number of bits to shift left.
		 *
		 * @returns The result of shifting this object's bits n times to the
		 *  left.
		 */
		constexpr big_unsigned operator<<(size_t n) const
		{
			big_unsigned result(*this);
			result <<= n;
			return result;
		}

		/** Right logical shift assignment operator.
		 *
		 * @param[in] n Number of bits to shift right.
		 *
		 * @returns A reference to this object.
		 */
		constexpr big_unsigned& operator>>=(size_t n)
		{
			for (size_t i = 0; i < n; ++i)
			{
				int prev_carry = 0;
				for (size_t byte = 0; byte < bytes; ++byte)
				{
					int carry = !!(data_[bytes-1-byte] & 0x1);
					data_[bytes-1-byte] >>= 1;
					data_[bytes-1-byte] |= (prev_carry << 7);
					prev_carry = carry;
				}
			}
			return *this;
		}

		/** Right logical shift operator.
		 *
		 * @param[in] n Number of bits to shift right.
		 *
		 * @returns The result of shifting this object's bits n times to the
		 *  right.
		 */
		constexpr big_unsigned operator>>(size_t n) const
		{
			big_unsigned result(*this);
			result >>= n;
			return result;
		}

		/** OR assignment operator.
		 *
		 * @param[in] number Number to OR to this object.
		 *
		 * @returns A reference to this object.
		 */
		constexpr big_unsigned& operator|=(const big_unsigned& number)
		{
			for (size_t i = 0; i < bytes; ++i)
			{
				data_[i] |= number.data_[i];
			}
			return *this;
		}

		/** OR operator.
		 *
		 * @param[in] number Number to OR with.
		 *
		 * @returns The result of OR between this object and number.
		 */
		constexpr big_unsigned operator|(const big_unsigned& number) const
		{
			big_unsigned result(*this);
			result |= number;
			return result;
		}

		/** Comparison operator.
		 *
		 * @param[in] number Number to compare against.
		 *
		 * @returns std::strong_ordering::* values, see C++20 <=> documentation
		 *  for more information.
		 */
		constexpr
		std::strong_ordering operator<=>(const big_unsigned& number) const
		{
			size_t idx = data_.size() - 1;
			for (size_t i = 0; i < data_.size(); ++i, --idx)
			{
				if ((data_[idx] <=> number.data_[idx]) != 0)
					break;
			}
			return data_[idx] <=> number.data_[idx];
		}

		/** Default equality operator
		 *
		 * @returns True if both numbers are equal, false otherwise.
		 */
		constexpr bool operator==(const big_unsigned&) const = default;

		/** Get a reference to the internal data array.
		 *
		 * @returns A reference to the internal data array. Lowest indexes are
		 *  lowest bits of the number (in other words, little endian).
		 */
		constexpr const value_type& get_data() const
		{
			return data_;
		}

	private:
		value_type data_;
	};
} // namespace firmutils

#endif//FIRMUTILS_BIG_UNSIGNED_H_

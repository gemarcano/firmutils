// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020
///@file

#ifndef FIRMUTILS_UTILS_H_
#define FIRMUTILS_UTILS_H_

#include <variant>
#include <string_view>
#include <type_traits>

namespace firmutils
{
	/** Convert string to a value. Must be specialized for specific types.
	 *
	 * Throws std::invalid_argument if no conversion is possible.
	 *
	 * @tparam T Type of target value.
	 *
	 * @param[in] str String to convert.
	 *
	 * @return Converted value.
	 */
	template<class T>
	T from_string(std::string_view str)
	{
		return T::unimplemented;
	}

	/** Represents a result of an operation that can fail.
	 *
	 * This class acts like a variant, as it can either be in a good state, in
	 * which case data of type T can be accessed, or it is in an error state,
	 * in which case only the error of type E can be accessed.
	 *
	 * @tparam T Type of value returned on success.
	 * @tparam E Type of value returned on failure.
	 */
	template<class T, class E>
	class result_type
	{
	public:
		// Make sure T and E aren't the same type
		static_assert(!std::is_same_v<std::remove_cvref_t<T>,
				std::remove_cvref_t<E>>,
			"result_type template parameters cannot be the same type");

		/** Default constructor */
		constexpr result_type() = default;

		/** Initialize with the actual value or error value.
		 *
		 * Using perfect forwarding to simplify implementation.
		 *
		 * @tparam A Type of argument (should be T or E).
		 *
		 * @param[in,out] value_error Value or error to either pass in or move
		 *  (depends on A being rvalue or lvalue).
		 */
		template<class A>
		constexpr explicit result_type(A&& value_error)
		:data(std::forward<A>(value_error))
		{
			static_assert(
				std::is_same_v<
					std::remove_cvref_t<A>, std::remove_cvref_t<T>> ||
				std::is_same_v<
					std::remove_cvref_t<A>, std::remove_cvref_t<E>>,
				"parameter must be one of the types of the result_type");
		}

		/** Assigns the error or value to this object.
		 *
		 * @param[in,out] value_error Value or error to copy or move into this
		 *  object, depending on whether it is an lvalue or an rvalue.
		 *
		 * @returns A reference to this object.
		 */
		template<class A>
		constexpr result_type& operator=(A&& value_error)
		{
			static_assert(
				std::is_same_v<
					std::remove_cvref_t<A>, std::remove_cvref_t<T>> ||
				std::is_same_v<
					std::remove_cvref_t<A>, std::remove_cvref_t<E>>,
				"parameter must be one of the types of the result_type");

			data = std::forward<A>(value_error);
			return *this;
		}

		/** Returns whether the value stored is a value or an error.
		 *
		 * @returns True if the value stored is not an error, false otherwise.
		 */
		constexpr operator bool() const
		{
			return data.index() == 0;
		}

		/** Gets the value stored.
		 *
		 * Throws an std::bad_variant_access exception if data isn't
		 *  active/loaded.
		 *
		 * @returns The value stored.
		 */
		constexpr T& get()
		{
			return std::get<T>(data);
		}

		/** Gets the error stored.
		 *
		 * Throws an std::bad_variant_access exception if error isn't
		 * active/loaded.
		 *
		 * @returns The error stored.
		 */
		constexpr E& error()
		{
			return std::get<E>(data);
		}
	private:
		std::variant<T, E> data;
	};
} // namespace firmutils

#endif//FIRMUTILS_UTILS_H_

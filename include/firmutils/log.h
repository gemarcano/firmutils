// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020
///@file

#ifndef FIRMUTILS_LOG_H_
#define FIRMUTILS_LOG_H_

#include <firmutils/utils.h>

#include <string_view>
#include <ostream>
#include <iostream>
#include <vector>
#include <algorithm>

namespace firmutils
{
	/** Describes log levels */
	enum class log_level
	{
		FATAL, WARN, INFO, VERBOSE, DEBUG
	};

	/** Converts a log_level to a string.
	 *
	 * @param[in] level log_level to convert to a string.
	 *
	 * @returns The string representation of the given log_level.
	 */
	constexpr std::string_view to_string(log_level level)
	{
		switch (level)
		{
		case log_level::FATAL:
			return "FATAL";
		case log_level::WARN:
			return "WARN";
		case log_level::INFO:
			return "INFO";
		case log_level::VERBOSE:
			return "VERBOSE";
		case log_level::DEBUG:
		default:
			return "DEBUG";
		}
	}

	/** Converts the given string to a log_level.
	 *
	 * Throws std::invalid_argument if no conversion could be done.
	 *
	 * @param[in] level log_level string to convert.
	 *
	 * @returns Converted log_level from string.
	 */
	template<>
	constexpr log_level from_string<log_level>(std::string_view level)
	{
		if (level == "FATAL")
			return log_level::FATAL;
		if (level == "WARN")
			return log_level::WARN;
		if (level == "INFO")
			return log_level::INFO;
		if (level == "VERBOSE")
			return log_level::VERBOSE;
		if (level == "DEBUG")
			return log_level::DEBUG;
		throw std::invalid_argument("Unknown log_level");
	}

	/** Container for log messages */
	using log_messages = std::vector<std::pair<log_level, std::string>>;

	/** Helper class to make a NULL ostream */
	class null_streambuf : public std::streambuf
	{
	public:
		/** See std::basic_streambuf::overflow. This implementation is set to
		 * implement a null ostream
		 *
		 * @param[in] c Character to "store" in put area (must not be
		 *  Traits::eof()).
		 *
		 * @returns The character value received.
		 */
		int overflow(int c) { return c; };
	};

	/** Represents a logger that manages logging output for different
	 *  log_levels and even different devices, depending on whether the output
	 *  is informational or a fatal error.
	 */
	class log_class
	{
	public:
		/** Constructor.
		 *
		 * @param[in] max_level Maximum log level to log (inclusive). e.g.
		 *  max_level as log_level::INFO will cause for log_levels FATAL, WARN,
		 *  and INFO to be logged, and everything else ignored.
		 * @param[in] out Stream to output logs that are not fatal.
		 * @param[out] err Stream to output logs that are fatal.
		 *
		 */
		explicit log_class(
				log_level max_level,
				std::ostream& out = std::cout,
				std::ostream& err = std::cerr)
		:max_level_(max_level), os_(out), es_(err), null_(&nstrbuf_)
		{}

		/** Gets the stream to log at the given level.
		 *
		 * @param[in] level Level to log at.
		 *
		 * @returns A stream that can be used to send log messages to. Newlines
		 *  are not appended automatically.
		 */
		std::ostream& log(log_level level)
		{
			unsigned u_level = static_cast<unsigned>(level);

			if (u_level <= static_cast<unsigned>(max_level_))
				return ((u_level < 2) ? es_ : os_) << to_string(level) << ": ";

			return null_;
		}

		/** Changes the maximum level to log.
		 *
		 * @param[in] max New max level (inclusive).
		 *
		 * @post Any logging requests for levels less verbose than and equal to
		 *  the specified maximum are logged, otherwise these are ignored.
		 */
		void set_max_level(log_level max)
		{
			max_level_ = max;
		}
	private:
		// 0 - FATAL
		// 1 - WARN
		// 2 - INFO
		// 3 - VERBOSE
		// 4 - DEBUG
		log_level max_level_;
		std::ostream& os_;
		std::ostream& es_;
		null_streambuf nstrbuf_;
		std::ostream null_;
	};

	/** Global log object */
	extern log_class log_object;

	/** Global log object logging function
	 *
	 * @param[in] level Level to log at.
	 *
	 * @returns A stream to be used for logging.
	 */
	inline std::ostream& logging(log_level level)
	{
		return log_object.log(level);
	}

	/** Sets the max log level for the global log instance.
	 *
	 * @param[in] max Maximum level to log.
	 *
	 * @post Any logging requests for levels less verbose than and equal to the
	 *  specified maximum are logged, otherwise these are ignored.
	 */
	inline void logging_set_max(log_level max)
	{
		log_object.set_max_level(max);
	}

	/** Represents a result object that includes a list of messages associated
	 *  with the result value.
	 *
	 * @tparam T Type of result.
	 */
	template<class T>
	class logged_result_type
	{
	public:
		/** Default construtor */
		logged_result_type() = default;

		/** Initializes the object with both the data and its associated
		 *  messages.
		 *
		 * @tparam A The type of the value (should be T)
		 * @tparam B The type of the messages (should be log_messages)
		 *
		 * @param[in,out] data The data to pass in for the value (moved if
		 *  rvalue).
		 * @param[in,out] messages The messages to pass in for the internal
		 *  messages (moved if rvalue).
		 */
		template<class A, class B>
		logged_result_type(A&& data, B&& messages)
		:data_(std::forward<A>(data)), messages_(std::forward<B>(messages))
		{}

		/** Assigns the data to this object.
		 *
		 * Using SFINAE to limit the forwarding to only things that are of
		 *  type T.
		 *
		 * @tparam A Should be the type of this object's value (T).
		 *
		 * @param[in,out] data Data to copy or move into this object (moved if
		 *  rvalue).
		 *
		 * @returns A reference to this object.
		 */
		template<class A>
		logged_result_type& operator=(
				std::enable_if_t<
					std::is_same_v<
						std::remove_cvref_t<A>, std::remove_cvref_t<T>>,
				A>&& data)
		{
			data = std::forward<A>(data);
			return *this;
		}

		/** Assigns the given messages to this object.
		 *
		 * Using SFINAE to limit the forwarding to only things that are of type
		 * log_messages.
		 *
		 * @tparam A Should be log_messages.
		 *
		 * @param[in,out] data Messages to copy or move into this object (moved
		 *  if rvalue).
		 *
		 * @returns A reference to this object.
		 */
		template<class A>
		logged_result_type& operator=(
				std::enable_if_t<
					std::is_same_v<
						std::remove_cvref_t<A>, log_messages>,
				A>&& data)
		{
			messages_ = std::forward<A>(data);
			return *this;
		}

		/** Returns whether the result indicates success or fatal failure.
		 *
		 * @returns True if the internal messages do not include a FATAL
		 *  message, false otherwise.
		 */
		operator bool() const
		{
			return !at_least(log_level::FATAL);
		}

		/** Returns true if there are any messages at the specified log level
		 *  or more critical.
		 *
		 * @param[in] level log_level to compare against.
		 *
		 * @returns True if there are any messages at the specified log level
		 *  or more critical, false otherwise.
		 */
		bool at_least(log_level level) const
		{
			auto check = [level](const log_messages::value_type& msg)
			{
				return check_failed(msg, level);
			};
			return std::any_of(messages_.begin(), messages_.end(), check);
		}

		/** Returns whether there are any messages at the specified log level.
		 *
		 * @param[in] level log_level to compare against.
		 *
		 * @returns True if there are any messages at the specified log level,
		 *  false otherwise.
		 */
		bool at_level(log_level level) const
		{
			auto check = [level](const log_messages::value_type& msg)
			{
				return msg.first == level;
			};
			return std::any_of(messages_.begin(), messages_.end(), check);
		}

		/** Add a message to this object.
		 *
		 * @tparam Args Type of the arguments to pass to log_messages.
		 *
		 * @param[in,out] args Arguments to copy or move into the internal
		 *  log_messages.
		 *
		 * @post Message is added to internal list of messages.
		 */
		template<class... Args>
		void emplace_msg(Args&&... args)
		{
			messages_.emplace_back(std::forward<Args>(args)...);
		}

		/** Returns a reference to the result's value.
		 *
		 * @returns A reference to the value.
		 */
		const T& get() const
		{
			return data_;
		}

		/** Returns a reference to the result's value.
		 *
		 * @returns A reference to the value.
		 */
		T& get()
		{
			return data_;
		}

		/** Returns a reference to the result's messages.
		 *
		 * @returns A reference to the messages.
		 */
		log_messages& messages()
		{
			return messages_;
		}

		/** Returns a reference to the result's messages.
		 *
		 * @returns A reference to the messages.
		 */
		const log_messages& messages() const
		{
			return messages_;
		}

	private:
		T data_;
		log_messages messages_;

		static bool check_failed(
				const log_messages::value_type& msg,
				log_level level)
		{
			using N = std::underlying_type_t<log_level>;
			return static_cast<N>(level) >= static_cast<N>(msg.first);
		}
	};
} // namespace firmutils

#endif//FIRMUTILS_LOG_H_

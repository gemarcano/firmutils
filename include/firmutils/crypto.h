// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020
///@file

#ifndef FIRMUTILS_CRYPTO_H_
#define FIRMUTILS_CRYPTO_H_

#include <firmutils/big_unsigned.h>

#include <array>
#include <vector>

namespace firmutils
{
	/** SIGHAX retail NAND signature */
	extern const std::array<unsigned char, 256> retail_nand_firm_sig;
	/** SIGHAX retail NAND NCSD header signature */
	extern const std::array<unsigned char, 256> retail_nand_ncsd_header_sig;
	/** SIGHAX retail SPI flash NTR FIRM signature */
	extern const std::array<unsigned char, 256> retail_spi_flash_ntr_firm_sig;
	/** SIGHAX dev NAND signature */
	extern const std::array<unsigned char, 256> dev_nand_firm_sig;
	/** SIGHAX dev NAND NCSD header signature */
	extern const std::array<unsigned char, 256> dev_nand_ncsd_header_sig;
	/** SIGHAX dev SPI flash NTR FIRM signature */
	extern const std::array<unsigned char, 256> dev_spi_flash_ntr_firm_sig;

	/** Retail SPI encryption key */
	extern const std::array<unsigned char, 16> retail_spi_key;
	/** Dev SPI encryption key */
	extern const std::array<unsigned char, 16> dev_spi_key;

	/** Calculates the SHA256 checksum/digest of the given data.
	 *
	 * @param[in] data Data to calculate checksum of.
	 *
	 * @returns The hash checksum/digest of the data.
	 */
	std::array<unsigned char, 0x20> sha256sum(
		const std::vector<unsigned char>& data);

	/** CBC 128 bit encryption.
	 *
	 * @pre data is a length a multiple of 16 bytes.
	 *
	 * @param[in] key Key for encryption.
	 * @param[in] iv Initialization vector for encryption.
	 * @param[in] data Plaintext data for encryption. The number of bytes of
	 *  data must be a multiple of 16. If it's not, the data is padded with 0
	 *  so that it is.
	 *
	 * @returns The encrypted cyphertext. An empty vector is returned on
	 *  failure (usually due to a problem with the underlying cryptographic
	 *  routines or being out of memory).
	 */
	std::vector<unsigned char> cbc128_encrypt(
			const std::array<unsigned char, 16>& key,
			const std::array<unsigned char, 16>& iv,
			const std::vector<unsigned char>& data);

	/** CBC 128 bit decryption.
	 *
	 * @pre data is a length a multiple of 16 bytes.
	 *
	 * @param[in] key Key for decryption.
	 * @param[in] iv Initialization vector for decryption.
	 * @param[in] data Cyphertext data for decryption. The number of bytes of
	 *  data must be a multiple of 16. If it's not, the data is padded with 0
	 *  so that it is.
	 *
	 * @returns The decrypted cyphertext. An empty vector is returned on
	 *  failure (usually due to a problem with the underlying cryptographic
	 *  routines or being out of memory).
	 */
	std::vector<unsigned char> cbc128_decrypt(
			const std::array<unsigned char, 16>& key,
			const std::array<unsigned char, 16>& iv,
			const std::vector<unsigned char>& data);

	/** Nintendo 3DS X Y key scrambler, used to calculate normal key.
	 *
	 * @param[in] X X 128 bit key.
	 * @param[in] Y Y 128 bit key
	 *
	 * @returns The normal key of X and Y per the Nintendo 3DS scrambler
	 *  algorithm.
	 */
	constexpr big_unsigned<16> nintendo_3ds_keyscrambler(
		big_unsigned<16> X, const big_unsigned<16>& Y)
	{
		constexpr big_unsigned<16> C (
			{0x8A, 0x76, 0x52, 0x5D, 0xDC, 0x91, 0x45, 0x02, 0x08, 0x04, 0xFE,
				0xC5, 0xAA, 0xE9, 0xF9, 0x1F});
		rotate_bits_left(X, 2);
		X ^= Y;
		X += C;
		rotate_bits_left(X, 87);
		return X;
	}
} // namespace firmutils

#endif//FIRMUTILS_CRYPTO_H_
